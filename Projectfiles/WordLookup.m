//
//  WordLookup.m
//  QuickWords
//
//  Created by Logen Watkins on 9/6/12.
//
//

#import "WordLookup.h"

@implementation WordLookup

+ (id)sharedInstance {
    static dispatch_once_t onceToken;
    static WordLookup *shared = nil;
    dispatch_once(&onceToken, ^{
        shared = [[WordLookup alloc] init];
        // init stuff here if desired.
    });
    return shared;
}

// Overriding init
- (id)init
{
    if(self=[super init]) {
        // Inititialize the dictoinary
        NSMutableArray *wordsList = [[NSMutableArray alloc] init];
        NSString *wordsListFilepath = [[NSBundle mainBundle] pathForResource:@"EnglishWordList" ofType:@"txt"];
        NSString *stringsFromFile = [[NSString alloc]
                                     initWithContentsOfFile:wordsListFilepath
                                     encoding:NSUTF8StringEncoding
                                     error:NULL];
        if (nil == stringsFromFile) {
            NSLog(@"Error! StringsFile string is empty!");
        }
        for (NSString *word in [stringsFromFile componentsSeparatedByString:@"\n"]) {
            [wordsList addObject:word];
        }
        
        // Now that array of words is created, turn that into a NSSet to
        //  get access to the "member" method, which tells me if it exists or not
        dictSet = [[NSSet alloc] initWithArray:wordsList];
        if (0 == [dictSet count]) {
            NSLog(@"Error! dictionary has 0 objects!!");
        }
        NSLog(@"WATK -- dict has %d objects", [dictSet count]);
    }
    
    return self;
}

// Method to check if a word exists in dictionary
- (BOOL) wordExists:(NSString *)word
{
    return (([dictSet member:word]) ? YES : NO);
}

- (void)dealloc {
    //empty
}

@end