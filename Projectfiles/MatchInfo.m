//
//  MatchInfo.m
//  QuickWords
//
//  Created by Logen Watkins on 9/9/12.
//
//

#import "MatchInfo.h"
#import "GameLayer.h"

@implementation MatchInfo

@synthesize matchID;

// Current thought process is that you'd call this to set up
//  a new match. This sets up all the gameEntities, which are passed to GameLayer
- (id) initToCreateNewMatch:(int)paramList
{
    if( (self = [super init]) ) {
        NSLog(@"Creating new match info");
        
        // Create new match information
        matchID = [self generateUniqueMatchID];
        NSLog(@"matchID generated: %@", matchID);
        
        // Initialize the current round
        currentRound = [NSNumber numberWithInt:FIRST_ROUND];
        
        // Initialize the starting player
        currentPlayer = [NSNumber numberWithInt:PLAYER_1];
        
        // Set up localContext
        NSManagedObjectContext *localContext = [NSManagedObjectContext MR_defaultContext];
        if (nil == localContext) {
            NSLog(@"Couldn't get localContext");
        }
        
        // Set up the matchEntity
        MatchInfoEntity *newMatch = [MatchInfoEntity MR_createInContext:localContext];
        if (nil == newMatch) {
            NSLog(@"Couldn't create a matchEntity");
        }
        newMatch.matchID = matchID;
        newMatch.currentRound = currentRound;
        newMatch.currentPlayer = currentPlayer;
        newMatch.lastMoveDate = [NSDate date];
        newMatch.matchComplete = [NSNumber numberWithBool:NO];
        
        // Create and save the gameEntities for every game
        int roundIterator;
        int playerIterator;
        for (roundIterator = 0; roundIterator < NUMBER_OF_ROUNDS; roundIterator++) {
            for (playerIterator = 0; playerIterator < NUMBER_OF_PLAYERS; playerIterator++) {
                GameInfoEntity *newGame = [GameInfoEntity MR_createInContext:localContext];
                if (nil == newGame) {
                    NSLog(@"Couldn't create a gameEntity");
                }
                newGame.roundNum = [NSNumber numberWithInt:roundIterator];
                newGame.playerNum = [NSNumber numberWithInt:playerIterator];
                NSString *newPromptLetters = [self generatePromptLetters:roundIterator];
                newGame.promptLetters = newPromptLetters;
                newGame.score = [NSNumber numberWithInt:0];
                newGame.inMatch = newMatch;
                NSLog(@"Created round #%d, game #%d", roundIterator, playerIterator);
            }
        }

        // Save match and games data to CoreData
        [localContext MR_save];
        NSLog(@"All games for this new match saved to CoreData");
    }
    
    return self;
}

// Use this init when you have a matchID already, and want to fill out the class
- (id) initFromPreviousMatch:(NSNumber *)paramMatchID
{
    if( (self = [super init]) ) {
        NSLog(@"Setting up MatchInfo for previous match: %@", paramMatchID);
        
        // Fetch this match from coredata watknotes -- of course need error checking
        // Set up localContext
        NSManagedObjectContext *localContext = [NSManagedObjectContext MR_defaultContext];
        if (nil == localContext) {
            NSLog(@"Error! Couldn't get localContext");
        }
        MatchInfoEntity *newMatch = [MatchInfoEntity MR_findFirstByAttribute:@"matchID"
                                                                   withValue:paramMatchID
                                                                   inContext:localContext];
        if (nil == newMatch) {
            NSLog(@"Error! Couldn't find match: %@", paramMatchID);
        }
        
        // Create new match information
        matchID = paramMatchID;
        
        // Initialize the current round
        currentRound = [newMatch currentRound];

        // Initialize the starting player
        currentPlayer = [newMatch currentPlayer];
    }
    
    return self;
}

- (int) visibleToUserRound_int:(int)round
{
    return (round + 1);
}
- (int) visibleToUserRound_NSN:(NSNumber *)round
{
    return ([round intValue] + 1);
}
- (int) visibleToUserPlayerNum_int:(int)playerNum
{
    return (playerNum + 1);
}
- (int) visibleToUserPlayerNum_NSN:(NSNumber *)playerNum
{
    return ([playerNum intValue] + 1);
}

+ (NSNumber *) getMatchIDForObjectID:(NSManagedObjectID *)paramObjectID
{
    // Set up localContext
    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_defaultContext];
    if (nil == localContext) {
        NSLog(@"Error! Couldn't get localContext");
    }
    MatchInfoEntity *curMatch = (MatchInfoEntity *)[localContext objectWithID:paramObjectID];
    if (nil == curMatch) {
        NSLog(@"Error! Couldn't find match");
    }
    
    return [curMatch matchID];
}

// Take a matchEntityID and update the appropriate entity
+ (void) setMatchComplete:(NSManagedObjectID *)matchEntityID
{
    // Set up localContext
    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_defaultContext];
    if (nil == localContext) {
        NSLog(@"Error! Couldn't get localContext");
    }
    // Get the match entity
    MatchInfoEntity *curMatch = (MatchInfoEntity *)[localContext objectWithID:matchEntityID];
    if (nil == curMatch) {
        NSLog(@"Error! Couldn't retrieve match");
    }
    
    curMatch.matchComplete = [NSNumber numberWithBool:YES];
    [localContext MR_save];
}

+ (void) setMatchCompleteByID:(NSNumber *)matchID
{
    // Set up localContext
    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_defaultContext];
    if (nil == localContext) {
        NSLog(@"Error! Couldn't get localContext");
    }
    // Get the match entity for this matchID
    MatchInfoEntity *curMatch = [MatchInfoEntity MR_findFirstByAttribute:@"matchID"
                                                               withValue:matchID
                                                               inContext:localContext];
    if (nil == curMatch) {
        NSLog(@"Error! Couldn't retrive match: %d", matchID);
    }
    [MatchInfo setMatchComplete:[curMatch objectID]];
}

- (NSNumber *)generateUniqueMatchID
{
    // Would need to get an unused matchID here
    // For now, just return a random number under 1000
    NSNumber *newID = [NSNumber numberWithLongLong:(arc4random() % 1000) + 1];
    return newID;
}

// Using "num" just to return something different
- (NSString *)generatePromptLetters:(int)num
{
    if (num == 0) {
        return @"ATE";
    } else if (num == 1) {
        return @"BOR";
    } else {
        return @"CAR";
    }
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
    
	// don't forget to call "super dealloc"
//	[super dealloc]; ARC disabled
}

@end
