//
//  RootMenuLayer.h
//  QuickWords
//
//  Created by Logen Watkins on 9/4/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//

// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"

#define LAYER_MIN_Y (0)
#define LAYER_MAX_Y (400) // this is a bad idea, make this something dynamic
#define MAX_TABLE_MATCHES 10

enum  {
    TABLEVIEW_CURRENTMATCHES = 0,
    TABLEVIEW_COMPLETEMATCHES, // 1
    TABLEVIEW_SECTION_COUNT // 2
};

// RootMenuLayer
@interface RootMenuLayer : CCLayer <CCTargetedTouchDelegate, UITableViewDataSource, UITableViewDelegate>
{
    NSManagedObjectContext *localContext;
    NSMutableArray *currentMatches;
    NSInteger maxCurrentMatches;
    NSMutableArray *completeMatches;
    NSInteger maxCompleteMatches;
    UITableView *matchesTable;
    CGSize winSize;
    KKInput *gestureInput;
    CCMenu *menu;
    CCMenuItem *itemPlayLocal;
    CCMenuItem *mine;
}

// returns a CCScene that contains the RootMenuLayer as the only child
+(CCScene *) scene;

@end
