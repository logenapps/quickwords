// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to MatchInfoEntity.h instead.

#import <CoreData/CoreData.h>


extern const struct MatchInfoEntityAttributes {
	__unsafe_unretained NSString *currentPlayer;
	__unsafe_unretained NSString *currentRound;
	__unsafe_unretained NSString *lastMoveDate;
	__unsafe_unretained NSString *matchComplete;
	__unsafe_unretained NSString *matchID;
} MatchInfoEntityAttributes;

extern const struct MatchInfoEntityRelationships {
	__unsafe_unretained NSString *hasGames;
} MatchInfoEntityRelationships;

extern const struct MatchInfoEntityFetchedProperties {
} MatchInfoEntityFetchedProperties;

@class GameInfoEntity;







@interface MatchInfoEntityID : NSManagedObjectID {}
@end

@interface _MatchInfoEntity : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (MatchInfoEntityID*)objectID;




@property (nonatomic, strong) NSNumber* currentPlayer;


@property int16_t currentPlayerValue;
- (int16_t)currentPlayerValue;
- (void)setCurrentPlayerValue:(int16_t)value_;

//- (BOOL)validateCurrentPlayer:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) NSNumber* currentRound;


@property int16_t currentRoundValue;
- (int16_t)currentRoundValue;
- (void)setCurrentRoundValue:(int16_t)value_;

//- (BOOL)validateCurrentRound:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) NSDate* lastMoveDate;


//- (BOOL)validateLastMoveDate:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) NSNumber* matchComplete;


@property BOOL matchCompleteValue;
- (BOOL)matchCompleteValue;
- (void)setMatchCompleteValue:(BOOL)value_;

//- (BOOL)validateMatchComplete:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) NSNumber* matchID;


@property int64_t matchIDValue;
- (int64_t)matchIDValue;
- (void)setMatchIDValue:(int64_t)value_;

//- (BOOL)validateMatchID:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet* hasGames;

- (NSMutableSet*)hasGamesSet;





@end

@interface _MatchInfoEntity (CoreDataGeneratedAccessors)

- (void)addHasGames:(NSSet*)value_;
- (void)removeHasGames:(NSSet*)value_;
- (void)addHasGamesObject:(GameInfoEntity*)value_;
- (void)removeHasGamesObject:(GameInfoEntity*)value_;

@end

@interface _MatchInfoEntity (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveCurrentPlayer;
- (void)setPrimitiveCurrentPlayer:(NSNumber*)value;

- (int16_t)primitiveCurrentPlayerValue;
- (void)setPrimitiveCurrentPlayerValue:(int16_t)value_;




- (NSNumber*)primitiveCurrentRound;
- (void)setPrimitiveCurrentRound:(NSNumber*)value;

- (int16_t)primitiveCurrentRoundValue;
- (void)setPrimitiveCurrentRoundValue:(int16_t)value_;




- (NSDate*)primitiveLastMoveDate;
- (void)setPrimitiveLastMoveDate:(NSDate*)value;




- (NSNumber*)primitiveMatchComplete;
- (void)setPrimitiveMatchComplete:(NSNumber*)value;

- (BOOL)primitiveMatchCompleteValue;
- (void)setPrimitiveMatchCompleteValue:(BOOL)value_;




- (NSNumber*)primitiveMatchID;
- (void)setPrimitiveMatchID:(NSNumber*)value;

- (int64_t)primitiveMatchIDValue;
- (void)setPrimitiveMatchIDValue:(int64_t)value_;





- (NSMutableSet*)primitiveHasGames;
- (void)setPrimitiveHasGames:(NSMutableSet*)value;


@end
