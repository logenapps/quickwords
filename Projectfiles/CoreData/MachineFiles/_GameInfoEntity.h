// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to GameInfoEntity.h instead.

#import <CoreData/CoreData.h>


extern const struct GameInfoEntityAttributes {
	__unsafe_unretained NSString *playerNum;
	__unsafe_unretained NSString *promptLetters;
	__unsafe_unretained NSString *roundNum;
	__unsafe_unretained NSString *score;
} GameInfoEntityAttributes;

extern const struct GameInfoEntityRelationships {
	__unsafe_unretained NSString *inMatch;
} GameInfoEntityRelationships;

extern const struct GameInfoEntityFetchedProperties {
} GameInfoEntityFetchedProperties;

@class MatchInfoEntity;






@interface GameInfoEntityID : NSManagedObjectID {}
@end

@interface _GameInfoEntity : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (GameInfoEntityID*)objectID;




@property (nonatomic, strong) NSNumber* playerNum;


@property int16_t playerNumValue;
- (int16_t)playerNumValue;
- (void)setPlayerNumValue:(int16_t)value_;

//- (BOOL)validatePlayerNum:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) NSString* promptLetters;


//- (BOOL)validatePromptLetters:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) NSNumber* roundNum;


@property int16_t roundNumValue;
- (int16_t)roundNumValue;
- (void)setRoundNumValue:(int16_t)value_;

//- (BOOL)validateRoundNum:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) NSNumber* score;


@property int16_t scoreValue;
- (int16_t)scoreValue;
- (void)setScoreValue:(int16_t)value_;

//- (BOOL)validateScore:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) MatchInfoEntity* inMatch;

//- (BOOL)validateInMatch:(id*)value_ error:(NSError**)error_;





@end

@interface _GameInfoEntity (CoreDataGeneratedAccessors)

@end

@interface _GameInfoEntity (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitivePlayerNum;
- (void)setPrimitivePlayerNum:(NSNumber*)value;

- (int16_t)primitivePlayerNumValue;
- (void)setPrimitivePlayerNumValue:(int16_t)value_;




- (NSString*)primitivePromptLetters;
- (void)setPrimitivePromptLetters:(NSString*)value;




- (NSNumber*)primitiveRoundNum;
- (void)setPrimitiveRoundNum:(NSNumber*)value;

- (int16_t)primitiveRoundNumValue;
- (void)setPrimitiveRoundNumValue:(int16_t)value_;




- (NSNumber*)primitiveScore;
- (void)setPrimitiveScore:(NSNumber*)value;

- (int16_t)primitiveScoreValue;
- (void)setPrimitiveScoreValue:(int16_t)value_;





- (MatchInfoEntity*)primitiveInMatch;
- (void)setPrimitiveInMatch:(MatchInfoEntity*)value;


@end
