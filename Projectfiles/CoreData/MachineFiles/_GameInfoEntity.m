// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to GameInfoEntity.m instead.

#import "_GameInfoEntity.h"

const struct GameInfoEntityAttributes GameInfoEntityAttributes = {
	.playerNum = @"playerNum",
	.promptLetters = @"promptLetters",
	.roundNum = @"roundNum",
	.score = @"score",
};

const struct GameInfoEntityRelationships GameInfoEntityRelationships = {
	.inMatch = @"inMatch",
};

const struct GameInfoEntityFetchedProperties GameInfoEntityFetchedProperties = {
};

@implementation GameInfoEntityID
@end

@implementation _GameInfoEntity

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"GameInfoEntity" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"GameInfoEntity";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"GameInfoEntity" inManagedObjectContext:moc_];
}

- (GameInfoEntityID*)objectID {
	return (GameInfoEntityID*)[super objectID];
}

+ (NSSet *)keyPathsForValuesAffectingValueForKey:(NSString *)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"playerNumValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"playerNum"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
	}
	if ([key isEqualToString:@"roundNumValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"roundNum"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
	}
	if ([key isEqualToString:@"scoreValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"score"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
	}

	return keyPaths;
}




@dynamic playerNum;



- (int16_t)playerNumValue {
	NSNumber *result = [self playerNum];
	return [result shortValue];
}

- (void)setPlayerNumValue:(int16_t)value_ {
	[self setPlayerNum:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitivePlayerNumValue {
	NSNumber *result = [self primitivePlayerNum];
	return [result shortValue];
}

- (void)setPrimitivePlayerNumValue:(int16_t)value_ {
	[self setPrimitivePlayerNum:[NSNumber numberWithShort:value_]];
}





@dynamic promptLetters;






@dynamic roundNum;



- (int16_t)roundNumValue {
	NSNumber *result = [self roundNum];
	return [result shortValue];
}

- (void)setRoundNumValue:(int16_t)value_ {
	[self setRoundNum:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveRoundNumValue {
	NSNumber *result = [self primitiveRoundNum];
	return [result shortValue];
}

- (void)setPrimitiveRoundNumValue:(int16_t)value_ {
	[self setPrimitiveRoundNum:[NSNumber numberWithShort:value_]];
}





@dynamic score;



- (int16_t)scoreValue {
	NSNumber *result = [self score];
	return [result shortValue];
}

- (void)setScoreValue:(int16_t)value_ {
	[self setScore:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveScoreValue {
	NSNumber *result = [self primitiveScore];
	return [result shortValue];
}

- (void)setPrimitiveScoreValue:(int16_t)value_ {
	[self setPrimitiveScore:[NSNumber numberWithShort:value_]];
}





@dynamic inMatch;

	






@end
