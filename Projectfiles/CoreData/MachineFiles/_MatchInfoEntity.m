// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to MatchInfoEntity.m instead.

#import "_MatchInfoEntity.h"

const struct MatchInfoEntityAttributes MatchInfoEntityAttributes = {
	.currentPlayer = @"currentPlayer",
	.currentRound = @"currentRound",
	.lastMoveDate = @"lastMoveDate",
	.matchComplete = @"matchComplete",
	.matchID = @"matchID",
};

const struct MatchInfoEntityRelationships MatchInfoEntityRelationships = {
	.hasGames = @"hasGames",
};

const struct MatchInfoEntityFetchedProperties MatchInfoEntityFetchedProperties = {
};

@implementation MatchInfoEntityID
@end

@implementation _MatchInfoEntity

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"MatchInfoEntity" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"MatchInfoEntity";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"MatchInfoEntity" inManagedObjectContext:moc_];
}

- (MatchInfoEntityID*)objectID {
	return (MatchInfoEntityID*)[super objectID];
}

+ (NSSet *)keyPathsForValuesAffectingValueForKey:(NSString *)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"currentPlayerValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"currentPlayer"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
	}
	if ([key isEqualToString:@"currentRoundValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"currentRound"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
	}
	if ([key isEqualToString:@"matchCompleteValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"matchComplete"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
	}
	if ([key isEqualToString:@"matchIDValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"matchID"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
	}

	return keyPaths;
}




@dynamic currentPlayer;



- (int16_t)currentPlayerValue {
	NSNumber *result = [self currentPlayer];
	return [result shortValue];
}

- (void)setCurrentPlayerValue:(int16_t)value_ {
	[self setCurrentPlayer:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveCurrentPlayerValue {
	NSNumber *result = [self primitiveCurrentPlayer];
	return [result shortValue];
}

- (void)setPrimitiveCurrentPlayerValue:(int16_t)value_ {
	[self setPrimitiveCurrentPlayer:[NSNumber numberWithShort:value_]];
}





@dynamic currentRound;



- (int16_t)currentRoundValue {
	NSNumber *result = [self currentRound];
	return [result shortValue];
}

- (void)setCurrentRoundValue:(int16_t)value_ {
	[self setCurrentRound:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveCurrentRoundValue {
	NSNumber *result = [self primitiveCurrentRound];
	return [result shortValue];
}

- (void)setPrimitiveCurrentRoundValue:(int16_t)value_ {
	[self setPrimitiveCurrentRound:[NSNumber numberWithShort:value_]];
}





@dynamic lastMoveDate;






@dynamic matchComplete;



- (BOOL)matchCompleteValue {
	NSNumber *result = [self matchComplete];
	return [result boolValue];
}

- (void)setMatchCompleteValue:(BOOL)value_ {
	[self setMatchComplete:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveMatchCompleteValue {
	NSNumber *result = [self primitiveMatchComplete];
	return [result boolValue];
}

- (void)setPrimitiveMatchCompleteValue:(BOOL)value_ {
	[self setPrimitiveMatchComplete:[NSNumber numberWithBool:value_]];
}





@dynamic matchID;



- (int64_t)matchIDValue {
	NSNumber *result = [self matchID];
	return [result longLongValue];
}

- (void)setMatchIDValue:(int64_t)value_ {
	[self setMatchID:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveMatchIDValue {
	NSNumber *result = [self primitiveMatchID];
	return [result longLongValue];
}

- (void)setPrimitiveMatchIDValue:(int64_t)value_ {
	[self setPrimitiveMatchID:[NSNumber numberWithLongLong:value_]];
}





@dynamic hasGames;

	
- (NSMutableSet*)hasGamesSet {
	[self willAccessValueForKey:@"hasGames"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"hasGames"];
  
	[self didAccessValueForKey:@"hasGames"];
	return result;
}
	






@end
