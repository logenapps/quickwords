//
//  MatchLayer.m
//  QuickWords
//
//  Created by Logen Watkins on 9/9/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "MatchLayer.h"
#import "RootMenuLayer.h"
#import "GameLayer.h"
#import "CCMenuItemLabelAndImage.h"

@implementation MatchLayer

// Implemented this singleton to allow passing of parameters. Necessary? Unknown.
+ (id)nodeWithParam:(MatchInfo *)paramList{
    return [[self alloc] initWithParam:paramList];
}

//  what kind of params might need here...
// probably give match id to pull right info?
-(id) initWithParam:(MatchInfo *)paramList
{
    if( (self = [super init]) ) {
        if (nil == paramList) {
            NSLog(@"Error! paramList to matchLayer Init is nil");
        } else {
            NSLog(@"Creating MatchLayer");
        }
        
        // Grab the window dimensions and hold onto them
        winSize = [[CCDirector sharedDirector] winSize];

        // Set background color
        CCLayerColor *backgroundLayer = [CCLayerColor layerWithColor:ccc4(255, 255, 255, 255)];
		[self addChild:backgroundLayer];
        
        // Hold onto the paramList
        matchSettings = paramList;
        
        // Create BACK button to return to home (temporary)
        CCMenuItem *itemBack = [CCMenuItemFont itemWithString:@"Back" block:^(id sender)
                                {
                                    [self makeTransitionToRootMenu];
                                }];
        backButton = [CCMenu menuWithItems:itemBack, nil];
        // Menu item color
        [backButton setColor:ccBLACK];
        // Position the menu
        [backButton setPosition:ccp(35,(winSize.height - 15))];
        [self addChild: backButton];
        
        // watknotes
        // Create a temporary button to set a match complete
        CCMenuItem *itemComplete = [CCMenuItemFont itemWithString:@"Mark Complete" block:^(id sender)
                                    {
                                        [MatchInfo setMatchCompleteByID:[matchSettings matchID]];
                                    }];
        CCMenu *completeButton = [CCMenu menuWithItems:itemComplete, nil];
        // Menu item color
        [completeButton setColor:ccBLACK];
        // Position the menu
        [completeButton setPosition:ccp(200,(winSize.height - 15))];
        [self addChild: completeButton];
        
        
        // Pull the appropriate match info from CoreData based on supplied matchID
        // Set up localContext
        NSManagedObjectContext *localContext = [NSManagedObjectContext MR_defaultContext];
        if (nil == localContext) {
            NSLog(@"Error! Couldn't get localContext");
        }
        MatchInfoEntity *curMatchEntity = [MatchInfoEntity MR_findFirstByAttribute:@"matchID"
                                                                         withValue:[matchSettings matchID]
                                                                         inContext:localContext];
        if (nil == curMatchEntity) {
            NSLog(@"Error! Couldn't retrieve match: %d", [matchSettings matchID]);
        }
        
        // Create the main score area
        NSManagedObjectID *curMatchID = (NSManagedObjectID *)[curMatchEntity objectID];
        if (nil == curMatchID) {
            NSLog(@"Error! couldn't get NSMO ID for match");
        }
        [self createScoreArea:curMatchID];
        
        // If rounds == NUMBER_OF_ROUNDS, display a total score/winner
        // Otherwise, show a button for the next player
        if (NUMBER_OF_ROUNDS == [curMatchEntity.currentRound intValue] ) {
            // watknotes calculate winner
            NSString *playLabelString = @"Winner/Score displayed here";
            CCLabelTTF *playLabel = [CCLabelTTF labelWithString:playLabelString fontName:@"Helvetica Neue" fontSize:28.0f];
            CCMenuItemLabelAndImage *itemPlay = [CCMenuItemLabelAndImage itemFromLabel:playLabel
                                                                           normalImage:@"topAndBottomRow.png"
                                                                         selectedImage:@"topAndBottomRowSelected.png"
                                                                         disabledImage:@"topAndBottomRow.png"];
            playButton = [CCMenu menuWithItems:itemPlay, nil];
            [playButton setColor:ccBLACK];
            // Position the menu
            [playButton setPosition:ccp((winSize.width * 0.5f), 50.0f)];
            [self addChild: playButton];
        } else {
            // Get the appropriate game entity to play next by looking at the
            //  matchID, currentRound and currentPlayer variables
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"inMatch == %@ AND playerNum == %@ AND roundNum == %@",
                                      curMatchID, curMatchEntity.currentPlayer, curMatchEntity.currentRound];
            nextGameEntity = [GameInfoEntity MR_findFirstWithPredicate:predicate inContext:localContext];
            NSLog(@"Looking up 'next game' with info -- matchID: %@, player: %@, round: %@",
                  curMatchEntity.matchID, curMatchEntity.currentPlayer, curMatchEntity.currentRound);
            if (nil == nextGameEntity) {
                NSLog(@"Error! Couldn't find appropriate game.");
            }
            else {
                NSLog(@"Success. Found gameEntity");
            }

            // create button for next game
            NSString *playLabelString = [NSString stringWithFormat:@"Round %d, Player %d!",
                                         [matchSettings visibleToUserRound_NSN: curMatchEntity.currentRound],
                                         [matchSettings visibleToUserPlayerNum_NSN:curMatchEntity.currentPlayer]];
            CCLabelTTF *playLabel = [CCLabelTTF labelWithString:playLabelString fontName:@"Helvetica Neue" fontSize:28.0f];
            CCMenuItemLabelAndImage *itemPlay = [CCMenuItemLabelAndImage itemFromLabel:playLabel
                                                                           normalImage:@"topAndBottomRow.png"
                                                                         selectedImage:@"topAndBottomRowSelected.png"
                                                                         disabledImage:@"topAndBottomRow.png"
                                                                                target:self
                                                                              selector:@selector(makeTransitionToGame)];
            playButton = [CCMenu menuWithItems:itemPlay, nil];
            [playButton setColor:ccBLACK];
            // Position the menu
            [playButton setPosition:ccp((winSize.width * 0.5f), 50.0f)];
            [self addChild: playButton];
        }
    }
    
    return self;
}

- (void) makeRoundArea:(int)round xPos:(float)curMenuX yPos:(float)curMenuY p1Score:(int)p1Score p2Score:(int)p2Score
{
    UIFont *curFont = [UIFont fontWithName:@"Helvetica Neue"
                                      size:20.0f];
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    
    // Display the Player scores for this round
    float playerScoreWidth = 60.0f;
    float playerScoreHeight = [curFont pointSize] + 3; // allows for commas
    
    // Create a label for Player 1 score
    NSString *player1ScoreString = [numberFormatter stringFromNumber:[NSNumber numberWithInt:p1Score]];
    CCLabelTTF *player1Label = [CCLabelTTF labelWithString:player1ScoreString
                                                dimensions:CGSizeMake(playerScoreWidth, playerScoreHeight)
                                                hAlignment:kCCTextAlignmentLeft
                                             lineBreakMode:kCCLineBreakModeClip
                                                  fontName:[curFont fontName]
                                                  fontSize:[curFont pointSize]];
    [player1Label setPosition:ccp(20.0f + (playerScoreWidth * 0.5f), curMenuY + 1)]; // offset Y to bring in line w/ Round #
    
    // Create a label for Player 2 score
    NSString *player2ScoreString = [numberFormatter stringFromNumber:[NSNumber numberWithInt:p2Score]];
    CCLabelTTF *player2Label = [CCLabelTTF labelWithString:player2ScoreString
                                                dimensions:CGSizeMake(playerScoreWidth, playerScoreHeight)
                                                hAlignment:kCCTextAlignmentRight
                                             lineBreakMode:kCCLineBreakModeClip
                                                  fontName:[curFont fontName]
                                                  fontSize:[curFont pointSize]];
    [player2Label setPosition:ccp((winSize.width - 20.0f) - (playerScoreWidth * 0.5f), curMenuY + 1)]; // offset Y to bring in line w/ Round #

    // Create the button (would probably go to a round details screen, but goes to a real game for now)
    // Also using the custom class to label it at the same time.
    int visibileToUserRound = [matchSettings visibleToUserRound_int:round]; // round 0 should be shown as round 1
    NSString *roundLabelText = [NSString stringWithFormat:@"Round %d", visibileToUserRound];

    CCLabelTTF *roundLabel = [CCLabelTTF labelWithString:roundLabelText
                                                fontName:[curFont fontName]
                                                fontSize:[curFont pointSize]];
    CCMenuItemLabelAndImage *roundButton = [CCMenuItemLabelAndImage itemFromLabel:roundLabel
                                                                      normalImage:@"darkgrey2.png"
                                                                    selectedImage:@"topAndBottomRowSelected.png"
                                                                    disabledImage:@"topAndBottomRow.png"
                                            ];
    [roundButton setPosition:ccp(curMenuX, curMenuY)];
    
    // All the objects are created. Color them
    // Color green if >= opponent, red if <
    if (p1Score == p2Score) {
        [player1Label setColor:ccBLUE];
        [player2Label setColor:ccBLUE];
    } else if (p1Score > p2Score) {
        [player1Label setColor:ccGREEN];
        [player2Label setColor:ccRED];
    } else {
        [player1Label setColor:ccRED];
        [player2Label setColor:ccGREEN];
    }
    
    // Add the objects in appropriate Z-order
    [self addChild:roundButton];
    [self addChild:player2Label];
    [self addChild:player1Label];
}

// Call this function, which calls another function to create each of the round areas
// The function to create each area is supplied a base location (center of that button's image),
//  and then labels are created to hold the relevant strings (score, "Round 1", score)
- (void) createScoreArea:(NSManagedObjectID *)curMatchID
{
    float curMenuX = winSize.width * 0.5f;
    float curMenuY = winSize.height * 0.7f;
    float scoreMenuHeight = 90.0f;

    // Set up localContext
    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_defaultContext];
    if (nil == localContext) {
        NSLog(@"Error! Couldn't retrieve localContext");
    }
    // Grab all the game data for this matchID
    NSArray *gamesData = [GameInfoEntity MR_findByAttribute:@"inMatch"
                                                  withValue:curMatchID
                                                 andOrderBy:@"roundNum"
                                                  ascending:YES
                                                  inContext:localContext];
    if (nil == gamesData) {
        NSLog(@"Error! gamesData is nil!");
        // watknotes return as a cheap failure
        return;
    } else if ((NUMBER_OF_ROUNDS * NUMBER_OF_PLAYERS) != [gamesData count]) {
        NSLog(@"Error! gamesData has %d objects, but %d were expected",
              [gamesData count], (NUMBER_OF_ROUNDS * NUMBER_OF_PLAYERS));
        // watknotes return as a cheap failure
        return;
    }
        
    int roundIterator;
    int gameIterator = 0;
    int p1Score, p2Score;
    for (roundIterator = 0; roundIterator < NUMBER_OF_ROUNDS; roundIterator++) {
        // Get the player scores
        // This is a 2-player assumption

        if (PLAYER_1 == [[[gamesData objectAtIndex:gameIterator] playerNum] intValue]) {
            // The players are in order in the array
            GameInfoEntity *gameData1 = [gamesData objectAtIndex:gameIterator];
            GameInfoEntity *gameData2 = [gamesData objectAtIndex:gameIterator + 1];
            p1Score = [[gameData1 score] intValue];
            p2Score = [[gameData2 score] intValue];
        } else {
            // The players are in reverse order in the array
            GameInfoEntity *gameData1 = [gamesData objectAtIndex:gameIterator + 1];
            GameInfoEntity *gameData2 = [gamesData objectAtIndex:gameIterator];
            p1Score = [[gameData1 score] intValue];
            p2Score = [[gameData2 score] intValue];
        }

        // Increase gameIterator by 2
        gameIterator += 2;
        
        // Make the individual round area
        [self makeRoundArea:(roundIterator) xPos:curMenuX yPos:curMenuY p1Score:p1Score p2Score:p2Score];
        
        // Adjust the height to make the next area positioned correctly
        curMenuY -= scoreMenuHeight;
    }

    //cleanup
  //watknotes  [gamesData release];
}

- (void) makeTransitionToGame
{
    GameInfo *paramList = [[GameInfo alloc] initWithParams:nextGameEntity];
    if (nil == paramList) {
        NSLog(@"Error! paramList retrieved for transition is nil");
    }
    NSLog(@"Begginning transition to game");
    CCScene *scene = [CCScene node];
    [scene addChild:[GameLayer nodeWithParam:paramList]];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:scene withColor:ccWHITE]];
}

- (void) makeTransitionToRootMenu
{
    NSLog(@"Beginning transtion to root menu");
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0
                                                                                 scene:[RootMenuLayer scene]
                                                                             withColor:ccWHITE]];
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
    
	// don't forget to call "super dealloc"
//	[super dealloc]; ARC disabled
}
@end
