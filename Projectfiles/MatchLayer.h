//
//  MatchLayer.h
//  QuickWords
//
//  Created by Logen Watkins on 9/9/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "MatchInfo.h"


@interface MatchLayer : CCLayer {
    GameInfoEntity *nextGameEntity;
    MatchInfo *matchSettings;
    CGSize winSize;
    CCMenu *backButton;
    CCMenu *playButton;
}

+ (id)nodeWithParam:(MatchInfo *)paramList;

@end
