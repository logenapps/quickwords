/*
 * Kobold2D™ --- http://www.kobold2d.org
 *
 * Copyright (c) 2010-2011 Steffen Itterheim. 
 * Released under MIT License in Germany (LICENSE-Kobold2D.txt).
 */

#import "kobold2d.h"

// RootMenuLayer
@interface IntroLayer : CCLayer
{
}

// returns a CCScene that contains the RootMenuLayer as the only child
+(CCScene *) scene;

@end
