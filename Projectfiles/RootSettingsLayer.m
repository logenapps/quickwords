//
//  RootSettingsLayer.m
//  QuickWords
//
//  Created by Logen Watkins on 9/4/12.
//
//

#import "RootSettingsLayer.h"
#import "RootMenuLayer.h"

@implementation RootSettingsLayer


// Implemented this singleton to allow passing of parameters. Necessary? Unknown.
+ (id)nodeWithParam:(int)paramList{
    return [[self alloc] initWithLevel:paramList];
}

// Logen's note -- this is all just sample code pulled from Cocos2d tutorial
// Slightly modified to get working (remove resource-dependencies)
-(id) initWithLevel:(int)paramList
{
	if (self = [super init]) {
        NSLog(@"Creating RootSettingsLayer", paramList);
        
		[CCMenuItemFont setFontName: @"American Typewriter"];
		[CCMenuItemFont setFontSize:18];
		CCMenuItemFont *title1 = [CCMenuItemFont itemWithString: @"Sound"];
		[title1 setIsEnabled:NO];
		[CCMenuItemFont setFontName: @"Helvetica Neue"];
		[CCMenuItemFont setFontSize:34];
		CCMenuItemToggle *item1 = [CCMenuItemToggle itemWithTarget:self selector:@selector(menuCallback:) items:
                                   [CCMenuItemFont itemWithString: @"On"],
                                   [CCMenuItemFont itemWithString: @"Off"],
                                   nil];
        
		[CCMenuItemFont setFontName: @"American Typewriter"];
		[CCMenuItemFont setFontSize:18];
		CCMenuItemFont *title2 = [CCMenuItemFont itemWithString: @"Music"];
		[title2 setIsEnabled:NO];
		[CCMenuItemFont setFontName: @"Helvetica Neue"];
		[CCMenuItemFont setFontSize:34];
		CCMenuItemToggle *item2 = [CCMenuItemToggle itemWithTarget:self selector:@selector(menuCallback:) items:
                                   [CCMenuItemFont itemWithString: @"On"],
                                   [CCMenuItemFont itemWithString: @"Off"],
                                   nil];
        
		[CCMenuItemFont setFontName: @"American Typewriter"];
		[CCMenuItemFont setFontSize:18];
		CCMenuItemFont *title3 = [CCMenuItemFont itemWithString: @"Quality"];
		[title3 setIsEnabled:NO];
		[CCMenuItemFont setFontName: @"Helvetica Neue"];
		[CCMenuItemFont setFontSize:34];
		CCMenuItemToggle *item3 = [CCMenuItemToggle itemWithTarget:self selector:@selector(menuCallback:) items:
                                   [CCMenuItemFont itemWithString: @"High"],
                                   [CCMenuItemFont itemWithString: @"Low"],
                                   nil];
        
		[CCMenuItemFont setFontName: @"American Typewriter"];
		[CCMenuItemFont setFontSize:18];
		CCMenuItemFont *title4 = [CCMenuItemFont itemWithString: @"Orientation"];
		[title4 setIsEnabled:NO];
		[CCMenuItemFont setFontName: @"Helvetica Neue"];
		[CCMenuItemFont setFontSize:34];
		CCMenuItemToggle *item4 = [CCMenuItemToggle itemWithTarget:self selector:@selector(menuCallback:) items:
                                   [CCMenuItemFont itemWithString: @"Off"], nil];
        
		NSArray *more_items = [NSArray arrayWithObjects:
                               [CCMenuItemFont itemWithString: @"33%"],
                               [CCMenuItemFont itemWithString: @"66%"],
                               [CCMenuItemFont itemWithString: @"100%"],
                               nil];
		// TIP: you can manipulate the items like any other NSMutableArray
		[item4.subItems addObjectsFromArray: more_items];
        
		// you can change the one of the items by doing this
		item4.selectedIndex = 2;

        CCLabelTTF *label = [CCLabelTTF labelWithString:@"go back" fontName:@"Helvetica Neue" fontSize:48];
//original		CCLabelBMFont *label = [CCLabelBMFont labelWithString:@"go back" fntFile:@"bitmapFontTest3.fnt"];
		CCMenuItemLabel *back = [CCMenuItemLabel itemWithLabel:label target:self selector:@selector(backCallback:)];
        
		CCMenu *menu = [CCMenu menuWithItems:
                        title1, title2,
                        item1, item2,
                        title3, title4,
                        item3, item4,
                        back, nil]; // 9 items.
		[menu alignItemsInColumns:
		 [NSNumber numberWithUnsignedInt:2],
		 [NSNumber numberWithUnsignedInt:2],
		 [NSNumber numberWithUnsignedInt:2],
		 [NSNumber numberWithUnsignedInt:2],
		 [NSNumber numberWithUnsignedInt:1],
		 nil
         ]; // 2 + 2 + 2 + 2 + 1 = total count of 9.
        
		[self addChild: menu];
		CGSize s = [[CCDirector sharedDirector] winSize];
		[menu setPosition:ccp(s.width/2, s.height/2)];
	}
    
	return self;
}

// Logen's note -- could call initWithLevel with a default value.
-(id) init
{
    NSLog(@"Error! Not implemented!!");
    return self;
}

-(void) menuCallback: (id) sender
{
    //watknotes pulled this from cocos2d code, sends up a warning
//	NSLog(@"selected item: %@ index:%u", [sender selectedItem], (unsigned int) [sender selectedIndex] );
}

-(void) backCallback: (id) sender
{
	CCScene *scene = [CCScene node];
	[scene addChild:[RootMenuLayer node]];
	[[CCDirector sharedDirector] replaceScene:scene];
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// don't forget to call "super dealloc"
//	[super dealloc]; ARC disabled
}

@end
