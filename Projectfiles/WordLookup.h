//
//  WordLookup.h
//  QuickWords
//
//  Created by Logen Watkins on 9/6/12.
//
//

#import <Foundation/Foundation.h>

@interface WordLookup : NSObject
{
    NSSet *dictSet;
}

+(id)sharedInstance;
- (BOOL) wordExists:(NSString *)word;

@end
