//
//  GameInfo.m
//  QuickWords
//
//  Created by Logen Watkins on 9/10/12.
//
//

#import "GameInfo.h"
#import "CoreData/HumanFiles/MatchInfoEntity.h"
#import "MatchInfo.h"

@implementation GameInfo

@synthesize matchID;
@synthesize thisGameEntity;
@synthesize secondsLeft;
@synthesize currentPromptLetter;
@synthesize currentScore;
@synthesize promptLetterIndexes;
@synthesize promptLetterCount;
@synthesize promptLetters;
@synthesize usedWordList;
@synthesize gameOver;

- (id) initWithParams:(GameInfoEntity *)gameEntity;
{
    if( (self = [super init]) ) {
        NSLog(@"Creating GameInfo");
        
        matchID = [[gameEntity inMatch] objectID];
        roundNum = [gameEntity roundNum];
        playerNum = [gameEntity playerNum];
                          
        // Grab the WordLookup instance
        validWordList = [WordLookup sharedInstance];
        if (nil == validWordList) {
            NSLog(@"validWordList not initialized correctly");
        }
     
        // The promptLetters were passed in through gameEntity
        promptLetters = gameEntity.promptLetters;
        
        // Grab the length of the string
        promptLetterCount = [NSNumber numberWithUnsignedInt:[promptLetters length]];
        if (0 == [promptLetterCount intValue]) {
            NSLog(@"Error! promptLetterCount = 0");
        }
                  
        // Establish the usedWordList
        usedWordList = [[NSMutableSet alloc] init];
        
        // Create an array of uints to hold the position of each promptLetter
        //  in the user's current guess.
        // This lets me know that the promptLetter was deleted, and not just another
        //  of the same letter.
        promptLetterIndexes = [[NSMutableArray alloc] init];
        int indexesIterator;
        for (indexesIterator = 0; indexesIterator < [promptLetterCount intValue]; indexesIterator++) {
            NSNumber *nextIterator = [NSNumber numberWithInt:LETTER_NOT_FOUND];
            [promptLetterIndexes addObject:nextIterator];
        }
        
        if ([promptLetterIndexes count] != [promptLetterCount unsignedIntValue]) {
            NSLog(@"Error! Incorrect number of promptLetterIndexes created");
        }
        
        // Set the starting score
        currentScore = 0;
        
        // secondsLeft is the round timer
        secondsLeft = 11;
        
        // gameOver is NO to start the game
        gameOver = NO;
        
        // These are the variables that get set for each new guess
        [self resetGameInfoVars];

    }
    
    return self;
}

// Take in seconds and return a string in M:SS format
- (NSString *) stringMSSFromSeconds:(int)totalSeconds
{
    int minutes = (totalSeconds / 60);
    int seconds = (totalSeconds % 60);
    
    return [NSString stringWithFormat:@"%d:%02d", minutes, seconds];
}

// Decrement secondsLeft
- (void) decrementSecondsLeft
{
    // Don't allow time to be negative
    if (secondsLeft > 0) {
        secondsLeft--;
        
        // if secondsLeft = 0, toggle gameover
        if (0 == secondsLeft) {
            gameOver = YES;
        }
    }
}

// Helper to increment the currentPromptLetter
- (void) incrementCurrentPromptLetter
{
    currentPromptLetter++;
}

//
- (int) intAtPromptLetterIndex:(int)position
{
    return [[promptLetterIndexes objectAtIndex:position] intValue];
}

// Use this to set the currentPromptLetter position of the indexes with
//  the position that letter was found in the user's guess
- (void) setPromptLetterIndexWithPosition:(int)position
{
    NSNumber *nextIterator = [NSNumber numberWithInt:position];
    [promptLetterIndexes replaceObjectAtIndex:currentPromptLetter withObject:nextIterator];
}

// Returns YES if the passed in character is equal to the next promptLetter
- (BOOL) guessCharIsNextPromptChar:(char)guessChar
{
    if ([promptLetters characterAtIndex:currentPromptLetter] == guessChar) {
        return YES;
    } else {
        return NO;
    }
}

// Take an iteratorNum, and set it to LETTER_NOT_FOUND to indicate
//  that a letter has not yet been found
- (void) resetPromptLetterIndex:(int)iteratorNum
{
    NSNumber *nextIterator = [NSNumber numberWithInt:LETTER_NOT_FOUND];
    [promptLetterIndexes replaceObjectAtIndex:iteratorNum withObject:nextIterator];
}

- (void) resetGameInfoVars
{
    // Reset promptLetterIndexes
    int promptIterator;
    for (promptIterator = 0; promptIterator < [promptLetterCount intValue]; promptIterator++) {
        [self resetPromptLetterIndex:promptIterator];
    }
    // Reset currentPromptLetter
    currentPromptLetter = 0;
}

// Calculate the worth of a word (only valid words passed here)
- (integer_t) calculateWordWorth:(NSString *)word
{
    uint wordLength = [word length];
    uint wordWorth = wordLength * wordLength;
    return wordWorth;
}

// Check if guess contains all prompt letters, in order
- (BOOL) promptLettersValid:(NSString *)guess
{
    NSUInteger guessLength = [guess length];
    NSUInteger promptLetterNum = 0;
    NSUInteger guessIterator;
    NSUInteger totalLetters = [[self promptLetterCount] unsignedIntValue];
    for (guessIterator = 0; guessIterator < guessLength; guessIterator++) {
        // check if the guess character equals the next promptLetter
        // uppercase the guess character, as it will be lowercase
        // The way i'm doing this isn't really fast
        if ([[guess uppercaseString] characterAtIndex:guessIterator] == [promptLetters characterAtIndex:promptLetterNum]) {
            // Found the prompt letter, look for next
            promptLetterNum++;
            
            // If all letters have been found, return TRUE
            if ((promptLetterNum) == totalLetters) {
                return YES;
            }
        }
    }
    
    // If function gets here, not a valid match
    return NO;
}

// All guesses are sent to this method.
// Possible return values
//  1) word hasn't been used before
//     -- for now, thinking add all
//  2) prompt letters check
//  3) guess in word list
- (NSInteger) guessValid:(NSString *)guess
{
    NSInteger retVal = GUESS_NOT_IN_WORDLIST;
    
    // 1) See if word has been used before
    if (nil == [[self usedWordList] member:guess]) {
        // 2) Make sure all prompt letters are used, in order
        if (YES == [self promptLettersValid:guess]) {
            // 3) Make sure it's a word in the wordList
            if (YES == [validWordList wordExists:guess]) {
                // If all conditions are true, then this is a valid, unused word
                // Add it to the usedWordList
                [[self usedWordList] addObject:guess];
                // Return GUESS_VALID for guessValid
                retVal = GUESS_VALID;
            } else {
                retVal = GUESS_NOT_IN_WORDLIST;
            }
        } else {
            retVal = GUESS_PROMPT_INCOMPLETE;
        }
    } else {
        retVal = GUESS_ALREADY_USED;
    }
    
    // If all conditions were not met, this will still be NO
    return retVal;
    
}

- (void) incrementToNextPlayerTurn:(NSManagedObjectContext *)context match:(MatchInfoEntity *)thisMatch
{
    // If currentPlayer = PLAYER_2, advance the round
    int curPlayer = [thisMatch.currentPlayer intValue];
    int curRound = [thisMatch.currentRound intValue];
    if (PLAYER_2 == curPlayer) {
        thisMatch.currentRound = [NSNumber numberWithInt:(curRound + 1)];
    }
    
    // If round = NUMBER_OF_ROUNDS
    if (NUMBER_OF_ROUNDS == [thisMatch.currentRound intValue]) {
        [MatchInfo setMatchComplete:[thisMatch objectID]];
    }
    
    // increment currentPlayer by adding 1 % 2 to the player
    thisMatch.currentPlayer = [NSNumber numberWithInt:((curPlayer + 1) % 2)];
    
    // Set the lastMoveDate to current
    thisMatch.lastMoveDate = [NSDate date];
    
    // Save it
    NSLog(@"Saving changes to Player & Turn");
    [context MR_save];
}

// Saves the current score to gameEntity, matchInfo to matchEntity to CoreData
- (void) saveGameScore:(NSManagedObjectContext *)context match:(MatchInfoEntity *)thisMatch
{
    // Grab the gameEntity by using the passed in gameEntity's stuff.
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"inMatch == %@ AND playerNum == %@ AND roundNum == %@",
                              matchID, playerNum, roundNum];
    GameInfoEntity *thisGame = [GameInfoEntity MR_findFirstWithPredicate:predicate inContext:context];
    if (nil == thisGame) {
        NSLog(@"Error! Couldn't retrieve gameEntity.");
    }
    thisGame.score = [NSNumber numberWithInt:currentScore];

    // Save it
    NSLog(@"Saving changes to GameScore");
    [context MR_save];
}

- (void) saveScoreAndIncrementTurn
{
    // Set up localContext
    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_defaultContext];
    if (nil == localContext) {
        NSLog(@"Error! Couldn't get localContext");
    }
    // Grab the matchEntity for this game
    MatchInfoEntity *thisMatch = (MatchInfoEntity *)[localContext objectWithID:matchID];
    if (nil == thisMatch) {
        NSLog(@"Error! Couldn't get matchEntity");
    }
    
    // Save the game score
    [self saveGameScore:localContext match:thisMatch];
    
    // Increment the player turn
    [self incrementToNextPlayerTurn:localContext match:thisMatch];
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
}


@end
