//
//  std.h
//  NewQuickWords
//
//  Created by Logen Watkins on 9/14/12.
//
//

#ifndef NewQuickWords_std_h
#define NewQuickWords_std_h

#define NUMBER_OF_PLAYERS 2
#define FIRST_ROUND 0
#define NUMBER_OF_ROUNDS 3
#define PLAYER_1 0
#define PLAYER_2 1

#endif
