//
//  CCRootMenuGameLabel.m
//  NewQuickWords
//
//  Created by Logen Watkins on 9/16/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "CCRootMenuGameLabel.h"
#import "CCMenuItem.h"


@implementation CCRootMenuGameLabel
@synthesize matchID;

- (void) loadLocalVars:(NSNumber *)pMatchID
{
    self.matchID = pMatchID;
}

+ (id) initWithImage:(NSString *)normImage selectedImage:(NSString *)selImage string1:(NSString *)string1 string2:(NSString *)string2 matchID:(NSNumber *)matchID target:(id)r selector:(SEL)s
{
    // The main node will be the menuItemImage
    CCRootMenuGameLabel *newMII = [CCRootMenuGameLabel itemWithNormalImage:normImage
                                                             selectedImage:selImage
                                                                    target:r
                                                                  selector:s];

    // Font to use
    UIFont *curFont = [UIFont fontWithName:@"Helvetica Neue"
                                      size:20.0f];
    // Add the labels
    // Create label for string1
    CGSize labelSize = CGSizeMake(300.0f, [curFont pointSize] + 5.0);
    CCLabelTTF *string1Label = [CCLabelTTF labelWithString:string1
                                                dimensions:labelSize
                                                hAlignment:kCCTextAlignmentLeft
                                                  fontName:[curFont fontName]
                                                  fontSize:[curFont pointSize]];
    string1Label.position = ccp(labelSize.width * 0.5f + 10.0, labelSize.height * 3);
    [string1Label setColor:ccBLUE];
    [newMII addChild:string1Label];
    
    // Create label for string2
    CCLabelTTF *string2Label = [CCLabelTTF labelWithString:string2
                                                dimensions:labelSize
                                                hAlignment:kCCTextAlignmentLeft
                                                  fontName:[curFont fontName]
                                                  fontSize:[curFont pointSize]];
    string2Label.position = ccp(labelSize.width * 0.5f + 10.0, labelSize.height * 2);
    [string2Label setColor:ccRED];
    [newMII addChild:string2Label];
    
    // Set the appropriate variables
    [newMII loadLocalVars:matchID];
    
    // Pass it all back
    return newMII;
}

@end
