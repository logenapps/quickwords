//
//  GameInfo.h
//  QuickWords
//
//  Created by Logen Watkins on 9/10/12.
//
//

#import <Foundation/Foundation.h>
#import "WordLookup.h"
#import "CoreData/HumanFiles/GameInfoEntity.h"

// Some random defines
#define LETTER_NOT_FOUND (-1)

// return options for user guesses
enum {
    GUESS_ALREADY_USED = -3,
    GUESS_PROMPT_INCOMPLETE = -2,
    GUESS_NOT_IN_WORDLIST = -1,
    GUESS_VALID = 1
};

@interface GameInfo : NSObject
{
    NSManagedObjectID *matchID;
    NSNumber *roundNum;
    NSNumber *playerNum;
    int secondsLeft;
    uint currentPromptLetter;
    int currentScore;
    NSMutableArray *promptLetterIndexes;
    NSNumber *promptLetterCount;
    NSString *promptLetters;
    NSMutableSet *usedWordList;
    BOOL gameOver;
    WordLookup *validWordList;
}

// At some point I'd like to break this a bit,
//  and change it so that methods are called instead of variables
//  being accessed directly...

@property (nonatomic, retain) NSManagedObjectID *matchID;
@property (nonatomic) int secondsLeft;
@property (nonatomic) uint currentPromptLetter;
@property (nonatomic) int currentScore;
@property (nonatomic, retain) GameInfoEntity *thisGameEntity;
@property (nonatomic, retain) NSMutableArray *promptLetterIndexes;
@property (nonatomic, retain) NSNumber *promptLetterCount;
@property (nonatomic, retain) NSString *promptLetters;
@property (nonatomic, retain) NSMutableSet *usedWordList;
@property (nonatomic) BOOL gameOver;

- (void) saveScoreAndIncrementTurn;
- (id) initWithParams:(GameInfoEntity *)gameEntity;
- (NSString *) stringMSSFromSeconds:(int)totalSeconds;
- (void) decrementSecondsLeft;
- (void) resetPromptLetterIndex:(int)iteratorNum;
- (integer_t) calculateWordWorth:(NSString *)word;
- (NSInteger) guessValid:(NSString *)guess;
- (BOOL) guessCharIsNextPromptChar:(char)guessChar;
- (void) setPromptLetterIndexWithPosition:(int)position;
- (void) incrementCurrentPromptLetter;
- (int) intAtPromptLetterIndex:(int)position;
- (void) resetGameInfoVars;
@end
