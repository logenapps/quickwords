/*
 * Kobold2D™ --- http://www.kobold2d.org
 *
 * Copyright (c) 2010-2011 Steffen Itterheim. 
 * Released under MIT License in Germany (LICENSE-Kobold2D.txt).
 */

#import "IntroLayer.h"
// Import the interfaces
#import "IntroLayer.h"
#import "RootMenuLayer.h"
#import "WordLookup.h"

@interface IntroLayer (PrivateMethods)
@end

// RootMenuLayer implementation
@implementation IntroLayer

// Helper class method that creates a Scene with the RootMenuLayer as the only child.
+(CCScene *) scene
{    
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	IntroLayer *layer = [IntroLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

//
-(void) onEnter
{
	[super onEnter];
    
	// ask director for the window size
	CGSize size = [[CCDirector sharedDirector] winSize];
    
	CCSprite *background;
	
	if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
		background = [CCSprite spriteWithFile:@"Default.png"];
        // was used when in landscape
        //		background.rotation = 90;
	} else {
		background = [CCSprite spriteWithFile:@"Default-Landscape~ipad.png"];
	}
	background.position = ccp(size.width/2, size.height/2);
    
	// add the label as a child to this Layer
	[self addChild: background];
    
    // Perform app initialization and then transition to the RootMenuLayer
    [self performInitAndTransition];
}

- (void) performInitAndTransition
{
    // Trying my init code here instead...
#ifdef TESTING
    // This can't go into production nowadays
    // This must be before takeoff to get a personable record (non-anonymous)
    [TestFlight setDeviceIdentifier:[[UIDevice currentDevice] uniqueIdentifier]];
#endif
    
    // Set up TestFlight
    [TestFlight takeOff:@"2d4d31d588377343f7e6118a4746e40f_MTMzMTY4MjAxMi0wOS0xNyAyMDoyNjoyNC43ODg4MTA"];
    
    // Set up Magical Record
//    [MagicalRecord setupCoreDataStackWithStoreNamed:@"MyDatabase.sqlite"]; //orig
    NSLog(@"Setting up CoreData Stack");
    [MagicalRecord setupCoreDataStack]; // switching to this method
    NSManagedObjectContext *myNewContext = [NSManagedObjectContext MR_context]; //logen added to support defaultcontext
    [NSManagedObjectContext MR_setDefaultContext:myNewContext]; // logen added to support defaultcontext

    
    // Set up wordlist
    NSLog(@"Setting up the WordList");
    WordLookup *validWordList = [WordLookup sharedInstance];
    if (nil == validWordList) {
        NSLog(@"validWordList not initialized correctly");
    }
    // Trying my init code here instead...
    
    // Transition to RootMenuLayer
    [self makeTransition];
}

-(void) makeTransition
{
	[[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.5 scene:[RootMenuLayer scene] withColor:ccBLACK]];
}

@end
