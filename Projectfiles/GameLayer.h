//
//  GameLayer.h
//  QuickWords
//
//  Created by Logen Watkins on 9/4/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "GameInfo.h"

#define GUESS_LABELS 20

@interface GameLayer : CCLayer <UITextFieldDelegate> {
    GameInfo *gameSettings;
    CGSize winSize;
    UITextField *inputArea;
    CCLabelTTF *inputLabel;
    CCLabelTTF *scoreLabel;
    CCLabelTTF *wordsCountTag;
    CCLabelTTF *wordsCountLabel;
    CCLabelTTF *secondsLeftLabel;
    CCMenu *backButton;
    CCMenu *readyButton;
    CCMenu *matchScreenButton;
    NSMutableArray *promptLetterLabels;
    NSNumberFormatter *numberFormatter;
    NSMutableArray *guessPopupLabels;

}

+ (id)nodeWithParam:(GameInfo *)paramList;

@end
