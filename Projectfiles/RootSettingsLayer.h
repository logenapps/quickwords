//
//  RootSettingsLayer.h
//  QuickWords
//
//  Created by Logen Watkins on 9/4/12.
//
//

#import "cocos2d.h"

@interface RootSettingsLayer : CCLayer
{
}

// Change this int to a list later
+ (id)nodeWithParam:(int)paramList;
-(void) menuCallback: (id) sender;
-(void) backCallback: (id) sender;

@end
