//
//  RootMenuTitleLayer.m
//  NewQuickWords
//
//  Created by Logen Watkins on 9/16/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "RootMenuTitleLayer.h"


@implementation RootMenuTitleLayer

// Eventually change it so that this layer's top elements are opaque so that the layer
//  below doesn't show as it passes underneath
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super's" return value
	if(self=[super init]) {
        NSLog(@"Creating RootMenuTitleLayer");
        
        // ask director for the window size
		CGSize winSize = [[CCDirector sharedDirector] winSize];
        
        // Set background color
        CCLayerColor *backgroundLayer = [CCLayerColor layerWithColor:ccc4(0, 0, 0, 0)]; //black
//        CCLayerColor *backgroundLayer = [CCLayerColor layerWithColor:ccc4(255, 255, 255, 255)]; //white
		[self addChild:backgroundLayer];
        
		// create and initialize a Label
		CCLabelTTF *titleLabel = [CCLabelTTF labelWithString:@"QuickWords" fontName:@"Helvetica Neue" fontSize:48];
		// position the label on the center of the screen
		titleLabel.position =  ccp( winSize.width * 0.5f , winSize.height * 0.9f );
		// add the label as a child to this Layer
		[self addChild:titleLabel];
        
        // Create a temporary button to set a match complete
        CCMenuItemImage *itemFeedback = [CCMenuItemImage itemWithNormalImage:@"TestFlightFeedback.png"
                                                               selectedImage:@"TestFlightFeedbackDark.png"
                                                                       block:^(id sender)
                                         {
                                             [TestFlight openFeedbackView];
                                         }];
        CCMenu *feedbackButton = [CCMenu menuWithItems:itemFeedback, nil];
        // Position the menu
        [feedbackButton setPosition:ccp(45,(winSize.height - 125))];
        [self addChild: feedbackButton];
    }
 
    return self;
}
@end
