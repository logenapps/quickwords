//
//  RootMenuLayer.m
//  QuickWords
//
//  Created by Logen Watkins on 9/4/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//


// Import the interfaces
#import "RootMenuLayer.h"
#import "RootMenuTitleLayer.h"
#import "RootSettingsLayer.h"
#import "MatchLayer.h"
#import "MatchInfo.h"
#import "CCRootMenuGameLabel.h"

// Needed to obtain the Navigation Controller
#import "AppDelegate.h"

#pragma mark - RootMenuLayer

// RootMenuLayer implementation
@implementation RootMenuLayer

// Helper class method that creates a Scene with the RootMenuLayer as the only child.
+(CCScene *) scene
{
    NSLog(@"Creating RootMenuLayer scene");
    
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
    
    // create the title layer at top, add to scene
    RootMenuTitleLayer *titleLayer = [RootMenuTitleLayer node];
    [scene addChild:titleLayer];
    
	// create the main layer, add to scene
	RootMenuLayer *layer = [RootMenuLayer node];
    [scene addChild: layer];
	
	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super's" return value
	if (self=[super init]) {
        NSLog(@"Creating RootMenu layer");
        
		// ask director for the window size
		winSize = [[CCDirector sharedDirector] winSize];
        
        // Set background color
        CCLayerColor *backgroundLayer = [CCLayerColor layerWithColor:ccc4(0, 0, 0, 0)]; //black
//        CCLayerColor *backgroundLayer = [CCLayerColor layerWithColor:ccc4(255, 255, 255, 255)]; //white
		[self addChild:backgroundLayer];

		// Default font size will be 28 points, Helvetica Neue
		[CCMenuItemFont setFontSize:22];
        [CCMenuItemFont setFontName:@"Helvetica Neue"];
        
  		// Root Play Menu Item using blocks
        itemPlayLocal = [CCMenuItemFont itemWithString:@"New Local Game" block:^(id sender) {
            // For now, let's create a matchInfo here to send to our MatchLayer
            int paramList = 3;
            MatchInfo *newMatch = [[MatchInfo alloc] initToCreateNewMatch:paramList];
            CCScene *scene = [CCScene node];
			[scene addChild:[MatchLayer nodeWithParam:newMatch]];
			[[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:scene withColor:ccWHITE]];
        }
                                     ];        
        CCMenuItem *itemSettings = [CCMenuItemFont
                                    itemWithString:@"Settings" block:^(id sender) {
            CCScene *scene = [CCScene node];
			[scene addChild:[RootSettingsLayer nodeWithParam:3]];
			[[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:scene withColor:ccWHITE]];
        }
                                    ];
        
        // Create menu object, add to layer
		menu = [CCMenu menuWithItems:itemPlayLocal, itemSettings, nil];
		[menu alignItemsVerticallyWithPadding:10.0f];
		[menu setPosition:ccp( winSize.width * 0.5f, winSize.height * 0.75f)];
		[self addChild:menu];
        
        // Set up localContext
        localContext = [NSManagedObjectContext MR_defaultContext];
        if (nil == localContext) {
            NSLog(@"Error! Couldn't get localContext");
        }
        
        // Get the data ready for the UITableView
        // Grab current matches (matchComplete = NO), turn into mutablearray for sorting by lastMoveDate
        NSArray *currentMatchesArray = [MatchInfoEntity MR_findByAttribute:@"matchComplete"
                                                                 withValue:[NSNumber numberWithBool:NO]
                                                                andOrderBy:@"lastMoveDate"
                                                                 ascending:NO
                                                                 inContext:localContext];
        if (nil == currentMatchesArray) {
            NSLog(@"Retrieved zero (0) currentMatches");
        } else {
            currentMatches = [NSMutableArray arrayWithArray:currentMatchesArray];
            NSLog(@"Retrieved %d matches for currentMatches list", [currentMatches count]);
        }
        
        // Grab complete matches (matchComplete = YES), turn into mutableArray for sorting by lastMoveDate
        NSArray *completeMatchesArray = [MatchInfoEntity MR_findByAttribute:@"matchComplete"
                                                                  withValue:[NSNumber numberWithBool:YES]
                                                                 andOrderBy:@"lastMoveDate"
                                                                  ascending:NO
                                                                  inContext:localContext];
        if (nil == completeMatchesArray) {
            NSLog(@"Retrieved zero (0) completeMatches");
        } else {
            completeMatches = [NSMutableArray arrayWithArray:completeMatchesArray];
            NSLog(@"Retrieved %d matches for completeMatches list", [completeMatches count]);
        }
        
        // Set a default value for maxCurrentMatches
        maxCurrentMatches = MAX_TABLE_MATCHES; // temp #
        maxCompleteMatches = MAX_TABLE_MATCHES; // temp #
        
        // Add current games uiTableView
        matchesTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 170, 300, 250) style:UITableViewStyleGrouped];
        matchesTable.dataSource = self;
        matchesTable.delegate = self;
        matchesTable.backgroundView = nil;
	}
	return self;
}

// current matches stuff
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // number of sections
    return TABLEVIEW_SECTION_COUNT;
}

//Header for each section
// set the height for the header
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    CGFloat curHeight = 0;
    switch (section) {
        case TABLEVIEW_CURRENTMATCHES:
            curHeight = 22;
            break;
        case TABLEVIEW_COMPLETEMATCHES:
            curHeight = 22;
            break;
        default:
            break;
    }
    
    return curHeight;
}
// draw the view for header
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *curView;
    UIFont *curFont = [UIFont fontWithName:@"Helvetica Neue" size:18.0];
    
    // Sharing this code
    CGRect curRect = CGRectMake(tableView.frame.origin.x + 24.0, (0), 175, [curFont pointSize] + 5.0);
    curView = [[UIView alloc] initWithFrame:curRect];
    curView.backgroundColor = [UIColor blackColor];
    UILabel *curLabel = [[UILabel alloc] initWithFrame:curRect];
    curLabel.font = [UIFont boldSystemFontOfSize:[curFont pointSize]];
    curLabel.textAlignment = UITextAlignmentLeft;

    switch (section) {
        case TABLEVIEW_CURRENTMATCHES:
        {
            // To be used in the view/label
            curLabel.text = @"Current Matches";
            curLabel.backgroundColor = [UIColor whiteColor];
            curLabel.textColor = [UIColor blueColor];
            [curView addSubview:curLabel];
            break;
        }
        case TABLEVIEW_COMPLETEMATCHES:
        {
            // Specific changes for this section
            curLabel.text = @"Completed Matches";
            curLabel.backgroundColor = [UIColor whiteColor];
            curLabel.textColor = [UIColor blueColor];
            [curView addSubview:curLabel];
            break;
        }
        default:
            curView = nil;
            break;
    }
    
    return curView;
}
// determine the number of rows to send back for each section
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger retVal;
    switch (section) {
        case TABLEVIEW_CURRENTMATCHES:
        {
            NSInteger currentMatchesReturned = [currentMatches count];
            if (currentMatchesReturned < maxCurrentMatches) {
                maxCurrentMatches = currentMatchesReturned;
            }
            retVal = maxCurrentMatches;
            break;
        }
        case TABLEVIEW_COMPLETEMATCHES:
        {
            NSInteger completeMatchesReturned = [completeMatches count];
            if (completeMatchesReturned < maxCompleteMatches) {
                maxCompleteMatches = completeMatchesReturned;
            }
            retVal = maxCompleteMatches;
            break;
        }
        default:
            retVal = 0;
            break;
    }
    // for test
    return retVal;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *currentMatchIdentifier = @"currentMatchIdentifier";
    static NSString *completeMatchIdentifier = @"completeMatchIdentifier";
    NSInteger indexRow = indexPath.row;
    UITableViewCell *cell;
    switch (indexPath.section) {
        case TABLEVIEW_CURRENTMATCHES:
        {
            if (indexRow < maxCurrentMatches) {
                cell = [tableView dequeueReusableCellWithIdentifier:currentMatchIdentifier];
                if (nil == cell) {
                    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:currentMatchIdentifier];
                }
                
                // Get this match info
                MatchInfoEntity *curMatchEntity = [currentMatches objectAtIndex:indexPath.row];
                NSNumber *curMatchID = [curMatchEntity matchID];
                
                // Set up the cell
                cell.textLabel.text = [NSString stringWithFormat:@"match: %@", curMatchID];
                cell.detailTextLabel.text = [NSString stringWithFormat:@"round: %@, player: %@", [curMatchEntity currentRound], [curMatchEntity currentPlayer]];
            }
            break;
        }
        case TABLEVIEW_COMPLETEMATCHES:
        {
            if (indexRow < maxCompleteMatches) {
                cell = [tableView dequeueReusableCellWithIdentifier:completeMatchIdentifier];
                if (nil == cell) {
                    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:completeMatchIdentifier];
                }
                
                // Get this match info
                MatchInfoEntity *curMatchEntity = [completeMatches objectAtIndex:indexPath.row];
                NSNumber *curMatchID = [curMatchEntity matchID];
                
                // Set up the cell
                cell.textLabel.text = [NSString stringWithFormat:@"match: %@", curMatchID];
                cell.detailTextLabel.text = [NSString stringWithFormat:@"round: %@, player: %@", [curMatchEntity currentRound], [curMatchEntity currentPlayer]];
            }
            
            break;
        }
        default:
            cell = nil;
            break;
    }

    return cell;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case TABLEVIEW_CURRENTMATCHES:
        {
            // Get current match info
            MatchInfoEntity *curMatchEntity = [currentMatches objectAtIndex:indexPath.row];
            if (nil == curMatchEntity) {
                NSLog(@"Couldn't retrieve match from currentMatch array");
            }
            NSNumber *curMatchID = [curMatchEntity matchID];
            [self loadSpecificMatch:curMatchID];
            break;
        }
        case TABLEVIEW_COMPLETEMATCHES:
        {
            // Get current match info
            MatchInfoEntity *curMatchEntity = [completeMatches objectAtIndex:indexPath.row];
            if (nil == curMatchEntity) {
                NSLog(@"Couldn't retrive match from completeMatch array");
            }
            NSNumber *curMatchID = [curMatchEntity matchID];
            [self loadSpecificMatch:curMatchID];
            break;
        }
        default:
            break;
    }
    
    return indexPath; // what's this do
}
// For deleting a row
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        switch (indexPath.section) {
            case TABLEVIEW_CURRENTMATCHES:
            {
                // what about related games?
                // Get current match entity
                MatchInfoEntity *curMatchEntity = [currentMatches objectAtIndex:indexPath.row];
                if (nil == curMatchEntity) {
                    NSLog(@"Couldn't retrieve match from currentMatch array");
                }
                NSManagedObject *matchToDelete = [localContext objectWithID:[curMatchEntity objectID]];
                if (nil == matchToDelete) {
                    NSLog(@"Couldn't retrive NSMO for match");
                }
                // Delete from CoreData
                [localContext deleteObject:matchToDelete];
                [localContext MR_save];
                
                // Delete from currentMatches array
                NSInteger curRow = indexPath.row;
                [currentMatches removeObjectAtIndex:curRow];
                
                // Delete from tableview
                [matchesTable deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                                           withRowAnimation:UITableViewRowAnimationFade];
                [matchesTable reloadData];
                break;
            }
            case TABLEVIEW_COMPLETEMATCHES:
            {
                // Get current match entity
                MatchInfoEntity *curMatchEntity = [completeMatches objectAtIndex:indexPath.row];
                if (nil == curMatchEntity) {
                    NSLog(@"Couldn't retrieve match from completeMatch array");
                }
                NSManagedObject *matchToDelete = [localContext objectWithID:[curMatchEntity objectID]];
                if (nil == matchToDelete) {
                    NSLog(@"Couldn't retrieve NSMO for match");
                }
                // Delete from CoreData
                [localContext deleteObject:matchToDelete];
                [localContext MR_save];
                
                // Delete from currentMatches array
                NSInteger curRow = indexPath.row;
                [completeMatches removeObjectAtIndex:curRow];
                
                // Delete from tableview
                [matchesTable deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                                    withRowAnimation:UITableViewRowAnimationFade];
                [matchesTable reloadData];
                break;
            }
            default:
                break;
        }
    }
}

// the CCRootMenuGameLabels all point here
- (void) loadSpecificMatch:(NSNumber *)curMatchID
{
    // To call the MatchLayer, I need to get MatchInfo. Do so using matchID
    NSLog(@"load match: %@", curMatchID);
    MatchInfo *curMatch = [[MatchInfo alloc] initFromPreviousMatch:curMatchID];
    if (nil == curMatch) {
        NSLog(@"Couldn't load match info");
    }
    CCScene *scene = [CCScene node];
    [scene addChild:[MatchLayer nodeWithParam:curMatch]];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:scene withColor:ccWHITE]];
}

// stuff that I don't want to happen until it's all loaded
- (void) onEnterTransitionDidFinish
{
    [super onEnterTransitionDidFinish];
    
    // add the things that aren't part of th cocosview
    [[[CCDirector sharedDirector] view] addSubview:matchesTable];
    
    // Start looking for these user gestures
//    gestureInput.gestureSwipeEnabled = YES;
//    gestureInput.gesturePanEnabled = YES;
}

// stuff that I want to happen immediately as we exit
- (void) onExitTransitionDidStart
{
    [super onExitTransitionDidStart];
    
    // remove the things that aren't part of the cocosview
    [matchesTable removeFromSuperview];
    
}
// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
}

@end
