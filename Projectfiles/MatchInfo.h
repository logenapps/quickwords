//
//  MatchInfo.h
//  QuickWords
//
//  Created by Logen Watkins on 9/9/12.
//
//

#import <Foundation/Foundation.h>

#import "CoreData/HumanFiles/MatchInfoEntity.h"
#import "CoreData/HumanFiles/GameInfoEntity.h"

@interface MatchInfo : NSObject
{
    NSNumber *matchID;
    NSNumber *currentRound;
    NSNumber *currentPlayer;
}

@property (nonatomic, retain) NSNumber *matchID;

- (id) initToCreateNewMatch:(int)paramList;
- (id) initFromPreviousMatch:(NSNumber *)paramMatchID;
- (int) visibleToUserRound_int:(int)round;
- (int) visibleToUserRound_NSN:(NSNumber *)round;
- (int) visibleToUserPlayerNum_int:(int)playerNum;
- (int) visibleToUserPlayerNum_NSN:(NSNumber *)playerNum;
+ (NSNumber *) getMatchIDForObjectID:(NSManagedObjectID *)paramObjectID;
+ (void) setMatchComplete:(NSManagedObjectID *)MatchInfoEntityID;
+ (void) setMatchCompleteByID:(NSNumber *)matchID;

@end
