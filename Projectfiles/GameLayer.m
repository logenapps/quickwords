//
//  GameLayer.m
//  QuickWords
//
//  Created by Logen Watkins on 9/4/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "GameLayer.h"
#import "RootMenuLayer.h"
#import "MatchLayer.h"

@implementation GameLayer
// Implemented this singleton to allow passing of parameters. Necessary? Unknown.
+ (id)nodeWithParam:(GameInfo *)paramList
{
    return [[self alloc] initWithParam:paramList];
}

// Notes to self
// inputArea is a textfield that will hold text the user types
// inputLabel is the text I show to the user (all caps, spaced)
-(id) initWithParam:(GameInfo *)paramList
{
    if( (self = [super init]) ) {
        NSLog(@"Creating new GameLayer");
        
        // Grab the window dimensions and hold onto them
        winSize = [[CCDirector sharedDirector] winSize];
        
        // Set background color
        CCLayerColor *backgroundLayer = [CCLayerColor layerWithColor:ccc4(255, 255, 255, 255)];
		[self addChild:backgroundLayer];
        
        // Store the paramList in gameSettings
        gameSettings = paramList; // is this alloc'd right
        
        // Set up the number formatter for this game
        numberFormatter = [[NSNumberFormatter alloc] init];
        [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
        
        // Any font that's not 'special' should be the following
        UIFont *curFont;
        NSString *stdFontName = @"Helvetica Neue";
        
        // Create Ready? button to begin (everything else should be drawn by now)
        CCMenuItem *itemReady = [CCMenuItemFont itemWithString:@"Ready?" block:^(id sender)
        {
            [self gameLoop];
        }];
        readyButton = [CCMenu menuWithItems:itemReady, nil];
        // Menu item color
        [readyButton setColor:ccBLACK];
        // Position the menu
        [readyButton setPosition:ccp(winSize.width/2, winSize.height/2)];
        [self addChild: readyButton];

        // Create BACK button to return to home (temporary)
        CCMenuItem *itemBack = [CCMenuItemFont itemWithString:@"Back" block:^(id sender)
        {
            [self makeTransitionToRootMenu];
        }];
        backButton = [CCMenu menuWithItems:itemBack, nil];
        // Menu item color
        [backButton setColor:ccBLACK];
        // Position the menu
        [backButton setPosition:ccp(35,(winSize.height - 15))];
        [self addChild: backButton];
        
        // Initialize the label that will hold the game timer
        curFont = [UIFont fontWithName:stdFontName
                                  size:32.0f];
        integer_t secondsLeftWidth = (75); //hardcoded
        integer_t secondsLeftHeight = [curFont pointSize];
        NSString *secondsLeftString = [gameSettings stringMSSFromSeconds:[gameSettings secondsLeft]];
        secondsLeftLabel = [CCLabelTTF labelWithString:secondsLeftString
                                            dimensions:CGSizeMake(secondsLeftWidth, secondsLeftHeight)
                                            hAlignment:kCCTextAlignmentCenter
                                              fontName:[curFont fontName]
                                              fontSize:[curFont pointSize]];
        [secondsLeftLabel setPosition:ccp((winSize.width / 2), (winSize.height - (secondsLeftHeight * 0.5f) -4))];
        [secondsLeftLabel setColor:ccMAGENTA];
        [secondsLeftLabel setVisible:NO];
        [self addChild:secondsLeftLabel];
        
        // Initialize the label that will hold the user's typing/guesses
        curFont = [UIFont fontWithName:stdFontName
                                  size:32.0f];
        integer_t inputLabelWidth = (winSize.width * 0.98f);
        integer_t inputLabelHeight = [curFont pointSize];
        inputLabel = [CCLabelTTF labelWithString:@""
                                      dimensions:CGSizeMake(inputLabelWidth, inputLabelHeight)
                                      hAlignment:kCCTextAlignmentCenter
                                        fontName:[curFont fontName]
                                        fontSize:[curFont pointSize]];
        // this doesn't really need to be dynamic, unless multi-res for iphone5?
        // even if it is dynamic, this probably isn't right, as it should be based on keybaord height probably
        [inputLabel setPosition:ccp((winSize.width / 2), (winSize.height * .55f))];
        [inputLabel setColor:ccBLUE];
        [self addChild:inputLabel];        
        
        // Create the NSMutableSet to hold all the promptLetterLabels
        promptLetterLabels = [[NSMutableArray alloc] init];
        
        // Create label to show the user's current score
        curFont = [UIFont fontWithName:stdFontName
                                  size:24.0f];
        NSString *scoreLabelText = [NSString stringWithFormat:@"%d", [gameSettings currentScore]];
        int scoreLabelWidth = 85;
        int scoreLabelHeight = [curFont pointSize] + 3; // Allow for commas
        scoreLabel = [CCLabelTTF labelWithString:scoreLabelText
                                      dimensions:CGSizeMake(scoreLabelWidth, scoreLabelHeight)
                                      hAlignment:kCCTextAlignmentRight
                                      vAlignment:kCCVerticalTextAlignmentBottom
                                        fontName:[curFont fontName]
                                        fontSize:[curFont pointSize]];
        [scoreLabel setPosition:ccp((winSize.width - (scoreLabelWidth * 0.5f) - 4), (winSize.height - (scoreLabelHeight / 2)))];
        [scoreLabel setColor:ccGREEN];
        [scoreLabel setVisible:NO];
        [self addChild:scoreLabel];
        
        // Create the label for the words tag
        curFont = [UIFont fontWithName:stdFontName
                                  size:14.0f];
        NSString *wordsTagText = @"Words";
        integer_t wordsTagWidth = 45;
        integer_t wordsTagHeight = [curFont pointSize] + 3;
        wordsCountTag = [CCLabelTTF labelWithString:wordsTagText
                                    dimensions:CGSizeMake(wordsTagWidth, wordsTagHeight)
                                    hAlignment:kCCTextAlignmentRight
                                    vAlignment:kCCVerticalTextAlignmentBottom
                                      fontName:[curFont fontName]
                                      fontSize:[curFont pointSize]];
        [wordsCountTag setPosition:ccp((winSize.width - (wordsTagWidth * 0.5f) - 4),
                                       (winSize.height - scoreLabelHeight - (wordsTagHeight * 0.5f)))];
        [wordsCountTag setColor:ccBLACK];
        [wordsCountTag setVisible:NO];
        [self addChild:wordsCountTag];

        // Create label to show the user's current wordCount
        curFont = [UIFont fontWithName:stdFontName
                                  size:16.0f];
        NSString *wordsLabelText = [NSString stringWithFormat:@"%d", [[gameSettings usedWordList] count]];
        int wordsLabelWidth = 25;
        int wordsLabelHeight = [curFont pointSize] +4; // Allow for commas
        wordsCountLabel = [CCLabelTTF labelWithString:wordsLabelText
                                           dimensions:CGSizeMake(wordsLabelWidth, wordsLabelHeight)
                                           hAlignment:kCCTextAlignmentRight
                                           vAlignment:kCCVerticalTextAlignmentBottom
                                             fontName:[curFont fontName]
                                             fontSize:[curFont pointSize]];        
        [wordsCountLabel setPosition:ccp((wordsCountTag.position.x - (wordsTagWidth * 0.5f) - (wordsLabelWidth * 0.5f) - 5),
                                          (winSize.height - scoreLabelHeight - (wordsLabelHeight / 2)))];
        [wordsCountLabel setColor:ccGREEN];
        [wordsCountLabel setVisible:NO];
        [self addChild:wordsCountLabel];
        
        // Create a separate CCLabelTTF for each of the "prompt letters" (letters given to user)
        // Then put all of these labels into an array, so that we can cycle through them easily
        //  if size, color, or any piece of them should change
        curFont = [UIFont fontWithName:stdFontName
                                  size:40.0f];
        // How spaced should the labels be?
        int promptLetterWidth = 40;
        // Calculate where the first label should go.
        // Start from middle of window, moving (n-1)/2 widths to the left
        float promptFirstLabelX = (winSize.width / 2.0f) - (([[gameSettings promptLetterCount] floatValue] - 1.0f) / 2.0f) * promptLetterWidth;
        int promptIterator;
        for (promptIterator = 0; promptIterator < [[gameSettings promptLetterCount] intValue]; promptIterator++) {
            CCLabelTTF *curLabel = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%c", [[gameSettings promptLetters] characterAtIndex:promptIterator]]
                                                    dimensions:CGSizeMake(promptLetterWidth, [curFont pointSize])
                                                    hAlignment:kCCTextAlignmentCenter
                                                      fontName:[curFont fontName]
                                                      fontSize:[curFont pointSize]];
            // Starting from base, add 'n' widths to get next X value
            [curLabel setPosition:ccp(promptFirstLabelX + (promptIterator * promptLetterWidth), winSize.height * 0.85f)];
            [curLabel setVisible:NO];
            [self addChild:curLabel];
            // Add to promptLetterLabels array
            [promptLetterLabels addObject:curLabel];
        }
        
        // Create the text field that the user will type answers in...which they will never see
        // All changes will be displayed to the user through inputLabel instead
        inputArea = [[UITextField alloc] initWithFrame:CGRectMake(winSize.width, winSize.height,1,1)];
        // clear text, autocapoff
        [inputArea setAutocorrectionType:UITextAutocorrectionTypeNo];
        // Keyboard type, returnkey, get keypresses
        [inputArea setKeyboardType:UIKeyboardTypeAlphabet];
        [inputArea setReturnKeyType:UIReturnKeyGo];
        [inputArea setDelegate:self];
        [[[CCDirector sharedDirector] view] addSubview:inputArea];
        
        // Set up the guessPopupLabels
        [self initGuessPopupLabels];
    }
    
    return self;
}

// Call this function to get the layer prepared for the next guess
- (void) resetGameLayerVars
{
    // Clear inputArea, inputLabel, currentPromptLetter
    [inputArea setText:@""];
    [inputLabel setString:@""];
    
    // Color the promptLetterLabels red
    [self setPromptLettersColor:ccRED];
}

- (void) gameLoop
{
    // Prepare vars for first guess
    [self resetGameLayerVars];
    
    // Remove the "Ready?" button -- temporarily just hiding it...
    [readyButton setVisible:NO];
    
    // Bring necessary labels into view (let's fade these in later)
    [secondsLeftLabel setVisible:YES];
    [self setPromptLetterLabelsVisibility:YES];
    [wordsCountTag setVisible:YES];
    [wordsCountLabel setVisible:YES];
    [scoreLabel setVisible:YES];
    
    // Select the text box, bringing up the keyboard
    [inputArea select:self];
    
    // Start the timer
    [self schedule:@selector(tick:) interval:1.0];
}

// Time ticks every second
- (void) tick:(ccTime) dt
{
    // Update secondsLeft
    [gameSettings decrementSecondsLeft];
    
    // Update the secondsLeftLabel
    [secondsLeftLabel setString:[gameSettings stringMSSFromSeconds:[gameSettings secondsLeft]]];
    
    // If time has run out, kick off gameEnd
    if (YES == [gameSettings gameOver]) {
        // Stop the game ticker
        // Start the timer
        [self unschedule:@selector(tick:)];
        [self gameEnd];
    }
}

- (void) gameEnd
{
    NSLog(@"gameEnd has been signaled");
    
    // Hide the keyboard
    [inputArea resignFirstResponder];
    
    // Fade actions
    id fadeIn = [CCFadeIn actionWithDuration:1.0f];
    id fadeOut = [CCFadeOut actionWithDuration:1.0f];
    
    // Hiding the backButton because we'll force the user to the match screen
    // Hiding the labels the user shouldn't see
    [self setPromptLetterLabelsVisibility:NO]; // make this dramatic later
    [inputLabel setString:@""]; //watknotes temp bc below isn't working
    [inputLabel runAction:fadeOut]; // this doesn't seem to be working...
    [backButton runAction:fadeOut]; // this doesn't seem to be working...
    
    // Create a new label that says "Game Over"
    UIFont *curFont = [UIFont fontWithName:@"Helvetica Neue"
                                      size:34.0f];
    NSString *gameOverLabelText = @"Game Over";
    int gameOverLabelWidth = (int)(winSize.width);
    CCLabelTTF *gameOverLabel = [CCLabelTTF labelWithString:gameOverLabelText
                                                 dimensions:CGSizeMake(gameOverLabelWidth, [curFont pointSize])
                                                 hAlignment:kCCTextAlignmentCenter
                                                   fontName:[curFont fontName]
                                                   fontSize:[curFont pointSize]];
    [gameOverLabel setPosition:ccp((winSize.width * 0.5f), (winSize.height * 0.75f))];
    [gameOverLabel setColor:ccRED];
    [gameOverLabel setOpacity:0];
    [self addChild:gameOverLabel];
    [gameOverLabel runAction:fadeIn];
    
    //Display a message to the user stating that the save is happening
    // Create a new label that says "Game Over"
    curFont = [UIFont fontWithName:@"Helvetica Neue"
                              size:22.0f];
    NSString *savingGameDataText = @"Saving game data...";
    int savingGameDataHeight = [curFont pointSize] + 5;
    CCLabelTTF *savingGameDataLabel = [CCLabelTTF labelWithString:savingGameDataText
                                                       dimensions:CGSizeMake(gameOverLabelWidth, savingGameDataHeight)
                                                       hAlignment:kCCTextAlignmentCenter
                                                         fontName:[curFont fontName]
                                                         fontSize:[curFont pointSize]];
    [savingGameDataLabel setPosition:ccp((winSize.width * 0.5f), (winSize.height * 0.5f))];
    [savingGameDataLabel setColor:ccRED];
    [savingGameDataLabel setVisible:YES];
    [self addChild:savingGameDataLabel];
    
    // Slide the statusBar out
    // watknotes my idea here would be to shift the "saving game" to the status bar,
    //  but I'm not sure how to do that yet... put the text there, make sure it stays for at least 1 second
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    // Save the data to CoreData
    [gameSettings saveScoreAndIncrementTurn];
    // Slide the statusBar out
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    
    
    // Change the above label to reflect that the game has been saved
    id changeString = [[CCCallBlock alloc] initWithBlock:^ {
        [savingGameDataLabel setString:@"Game has been saved."];
        [savingGameDataLabel setColor:ccBLUE];
    }];
    [savingGameDataLabel runAction:[CCSequence actions:fadeOut, changeString, fadeIn, nil]];
    
    //
    // Create MATCH button to return to home (temporary)
    CCMenuItem *itemMatchScreen = [CCMenuItemFont itemWithString:@"Match Screen" block:^(id sender)
                                   {
                                       [self makeTransitionToMatchScreen];
                                   }];
    matchScreenButton = [CCMenu menuWithItems:itemMatchScreen, nil];
    // Menu item color
    [matchScreenButton setColor:ccORANGE];
    // Position the menu
    [matchScreenButton setPosition:ccp((winSize.width * 0.5f),(winSize.height * 0.25f))];
    [matchScreenButton setOpacity:0];
    [self addChild: matchScreenButton];
    [matchScreenButton runAction:[CCFadeIn actionWithDuration:3.0f]];
}

// Accepts a BOOL to set visibility for all promptLetterLabels
- (void) setPromptLetterLabelsVisibility:(BOOL)visible
{
    int promptIterator;
    CCLabelTTF *curLabel;
    for (promptIterator = 0; promptIterator < [[gameSettings promptLetterCount] intValue]; promptIterator++) {
        curLabel = [promptLetterLabels objectAtIndex:promptIterator];
        [curLabel setVisible:visible];
    }
}

// For changing all prompLetterLabels color at one time
// Used when making red (reset), or blue (all letters used)
- (void) setPromptLettersColor:(ccColor3B)labelColor
{
    int promptIterator;
    CCLabelTTF *curLabel;
    for (promptIterator = 0; promptIterator < [[gameSettings promptLetterCount] intValue]; promptIterator++) {
        curLabel = [promptLetterLabels objectAtIndex:promptIterator];
        [curLabel setColor:labelColor];
    }
}

// If the user set a delete (this function is only called if there is
//  something to delete), this function is called. If the index to be
//  deleted is the last letterPrompt found, then decrement currentPromptLetter
//  and make that label red again
- (void) colorPrevPromptLetterLabel:(int)indexToDelete
{
    // Do nothing if no promptLetters have been found
    if ([gameSettings currentPromptLetter] > 0) {
        //watknotes working here
        int prevPromptLetterNum = [gameSettings currentPromptLetter] - 1;
        
        if ([gameSettings intAtPromptLetterIndex:prevPromptLetterNum] == indexToDelete) {
            // Clear out that previous promptLetter
            [gameSettings resetPromptLetterIndex:prevPromptLetterNum];

            // Set this label to red
            CCLabelTTF *curLabel = [promptLetterLabels objectAtIndex:[gameSettings currentPromptLetter] - 1];
            [curLabel setColor:ccRED];

            // If the letter we just made red was the final promptLetter, the other
            //  letters will still be green, and need to be made blue
            if (([gameSettings currentPromptLetter]) == [[gameSettings promptLetterCount] unsignedIntValue]) {
                int promptIterator;
                for (promptIterator = 0; promptIterator < prevPromptLetterNum; promptIterator++) {
                    curLabel = [promptLetterLabels objectAtIndex:promptIterator];
                    [curLabel setColor:ccBLUE];
                }
            }
        
        // Decrement currentPromptLetter
        [gameSettings setCurrentPromptLetter:prevPromptLetterNum];
        }
    }
}

// Use the currentPromptLetter int to check the user's last letter to
//  the next promptLetter they need to match on, coloring if found.
// If all promptLetters have been found, set the whole set to a color
// ASSUMES: Method assumes that the 'guess' parameter is uppercase
- (void) colorNextPromptLetterLabel:(NSString *)guess
{
    // If all prompt letters already found, return
    if ([gameSettings currentPromptLetter] == [promptLetterLabels count]) {
        return;
    }
    
    // If guess = the next prompt letter, color it blue
    CCLabelTTF *curLabel = [promptLetterLabels objectAtIndex:[gameSettings currentPromptLetter]];
    if (YES == [gameSettings guessCharIsNextPromptChar:[guess characterAtIndex:0]]) {
        // Record the location of this promptLetter in the promptLetterIndexes
        // The location to record is length of inputLabel, as it's value hasn't been updated yet
        [gameSettings setPromptLetterIndexWithPosition:[[inputLabel string] length]];
        
        // Increment the number of promptLetters found
        [gameSettings incrementCurrentPromptLetter];

        if ([gameSettings currentPromptLetter] == [[gameSettings promptLetterCount] unsignedIntValue]) {
            // All the prompt letters have been found, color the whole string.
            [self setPromptLettersColor:ccGREEN];
        } else {
            [curLabel setColor:ccBLUE];
        }
    }
}

// catches when Go/Enter is hit -- this is where a user submits a word
// I'm assuming there's only one text entry field so this should be fine for now
- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    NSInteger guessRet = GUESS_NOT_IN_WORDLIST;
    NSInteger wordScore = 0;
    
    // Only process this guess if the game isn't over (trying to prevent race)
    if (NO == [gameSettings gameOver]) {
        NSString *userGuess = [textField.text lowercaseString];
        // See if word is valid (not used, uses promptLetters, is in validWordList
        guessRet = [gameSettings guessValid:userGuess];
        switch (guessRet) {
            case GUESS_VALID:
            {
                // Temp logging if word is good or bad
                NSLog(@"user guess: '%@' is %s", userGuess, "GOOD");
                
                // Calculate word worth
                wordScore = [gameSettings calculateWordWorth:userGuess];
                // Add to current score
                NSInteger newScore = [gameSettings currentScore] + wordScore;
                [gameSettings setCurrentScore:newScore];
                // Update the scoreLabel
                [scoreLabel setString:[numberFormatter stringFromNumber:[NSNumber numberWithInt:[gameSettings currentScore]]]];
                // Update the wordsCountLabel
                NSString *newWordsCount = [NSString stringWithFormat:@"%d", [[gameSettings usedWordList] count]];
                [wordsCountLabel setString:newWordsCount];
                break;
            }
            default:
            {
                NSLog(@"user guess: '%@' is %s", userGuess, "BAD");
                break;
            }
        }
        // Pop up a message showing the success/failure
        [self guessValidityPopup:guessRet guess:userGuess wordWorth:wordScore];
        
        // Reset everything for the next user guess
        [self resetGameLayerVars];
        [gameSettings resetGameInfoVars];
    }
    
    // not sure where this return goes
    return YES;
}

// The message popup shown to the user after they make a guess
- (void) guessValidityPopup:(NSInteger)guessRet guess:(NSString *)userGuess wordWorth:(NSInteger)wordScore
{
    NSString *msgString;
    ccColor3B msgColor;
    float startX = winSize.width * 0.5;
    float startY = winSize.height * 0.55;
    float endY = startY + (winSize.height * 0.25);
    
    switch (guessRet) {
        case GUESS_ALREADY_USED: {
            msgString = [NSString stringWithFormat:@"'%@' already used...", userGuess];
            msgColor = ccRED;
            break;
        }
        case GUESS_PROMPT_INCOMPLETE: {
            msgString = [NSString stringWithFormat:@"'%@' not used...", [gameSettings promptLetters]];
            msgColor = ccRED;
            break;
        }
        case GUESS_NOT_IN_WORDLIST: {
            msgString = [NSString stringWithFormat:@"'%@' not a word...", userGuess];
            msgColor = ccRED;
            break;
        }
        case GUESS_VALID: {
            msgString = [NSString stringWithFormat:@"'%@' (+%d)", userGuess, wordScore];
            msgColor = ccGREEN;
            break;
        }
        default: {
            NSLog(@"Error! guessRet '%d' is not a valid choice", guessRet);
            return; // hacky
            break;
        }
    }

    // get a CCLabelTTf, and update with these details
    CCLabelTTF *msgLabel = [self getNextGuessPopupLabel];
    // Start by making opacity 0 in case it pulls an actively used label
    [msgLabel setOpacity:0];
    [msgLabel setString:msgString];
    [msgLabel setPosition:ccp(startX, startY)];
    [msgLabel setColor:msgColor];
    
    // Set up the animations that will be used by guessPopupLabels
    CCFiniteTimeAction *moveGuessPopupUp = [CCMoveTo actionWithDuration:2.0 position:ccp(startX, endY)];
    CCFiniteTimeAction *fadeInGuessPopup = [CCFadeIn actionWithDuration:0.3];
    CCAction *delayGuessPopup = [CCDelayTime actionWithDuration:1.0];
    CCAction *fadeOutGuessPopup = [CCFadeOut actionWithDuration:0.5];
    CCSequence *fadeInGuessPopupAndFadeOut = [CCSequence actions:
                                              fadeInGuessPopup,
                                              delayGuessPopup,
                                              fadeOutGuessPopup,
                                              nil];

    // Animate it to the user (fade in, move, fadeout)
    [msgLabel runAction:moveGuessPopupUp];
    [msgLabel runAction:fadeInGuessPopupAndFadeOut];
}

// Use this function to get the next CCLabelTTF, rather than having
//  an infinite number in play
- (CCLabelTTF *)getNextGuessPopupLabel
{
    static uint curLabelNum = 0;
    
    // pass the next label back from the guessPopupLabels array
    curLabelNum++;
    if (GUESS_LABELS == curLabelNum) {
        curLabelNum = 0;
    }
    return [guessPopupLabels objectAtIndex:curLabelNum];
}

// Init the guessPopupLabels array with a defined number of members
- (void) initGuessPopupLabels
{
    // Create an array to hold the GuessPopupLabels
    guessPopupLabels = [[NSMutableArray alloc] init];
    
    uint labelIterator;
    for (labelIterator = 0; labelIterator < GUESS_LABELS; labelIterator++) {
        CCLabelTTF *curLabel = [CCLabelTTF labelWithString:@""
                                                dimensions:CGSizeMake(winSize.width * 0.98, 30.0)
                                                hAlignment:kCCTextAlignmentLeft
                                                vAlignment:kCCVerticalTextAlignmentBottom
                                                  fontName:@"Helvetica Neue"
                                                  fontSize:14.0];
        // Add to view, invisible to start
        [guessPopupLabels addObject:curLabel];
        [self addChild:curLabel];
    }
}

// This method catches when any key is pressed.
- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // should change textsize based on number of characters
    
    // want all characters to be shown to user as uppercase
    NSString *upperString = [string uppercaseString];
    
    // Update the inputLabel (shows characters to user)
    NSString *newLabelValue;
    int curLabelLength = [[inputLabel string] length];
    // "Delete keypresses come in as blank strings
    if (0 == [string length]) {
        // Check if index to delete (length - 2) is one of the promptLetters
        int indexToDelete = curLabelLength - 2;
        [self colorPrevPromptLetterLabel:indexToDelete];
        
        // Now delete 2 characters (last char + space)
        newLabelValue = [[inputLabel string] substringToIndex:curLabelLength - 2];
    } else {
        // Wasn't a 'delete', work with the user's input
        // Update appropriate promptLetterLabel if the user's character
        //  matches the next promptLetter
        [self colorNextPromptLetterLabel:upperString];

        // Create the new string appropriately
        if (nil == [inputLabel string]) {
            newLabelValue = [NSString stringWithFormat:@"%@ ", upperString];
        } else {
            newLabelValue = [NSString stringWithFormat:@"%@%@ ", [inputLabel string], upperString];
        }
    }

    // Show the user the updated string
    [inputLabel setString:newLabelValue];

    // YES will keep the old text
    // NO otherwise
    return YES;
}

- (void) makeTransitionToRootMenu
{
    // Make keyboard disappear
    // typically this call wouldn't be here, you'd make transition from a in-game setting menu
    [inputArea resignFirstResponder];

    // Head back to root menu
    NSLog(@"Begin transition to root menu");
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0
                                                                                 scene:[RootMenuLayer scene]
                                                                             withColor:ccWHITE]];
}

- (void)makeTransitionToMatchScreen
{
    // To call the MatchLayer, I need to get MatchInfo. Do so using matchID
    NSNumber *curMatchID = [MatchInfo getMatchIDForObjectID:[gameSettings matchID]];
    if (nil == curMatchID) {
        NSLog(@"Error retrieving matchId for given relationship");
    } else {
        NSLog(@"Retrieved matchID: %@ for upcoming transition", curMatchID);
    }
    MatchInfo *curMatch = [[MatchInfo alloc] initFromPreviousMatch:curMatchID];
    if (nil == curMatch) {
        NSLog(@"Error retrieving matchInfo for match: %@", curMatchID);
    }
    
    NSLog(@"Begin transition to MatchScreen");
    CCScene *scene = [CCScene node];
    [scene addChild:[MatchLayer nodeWithParam:curMatch]];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:scene withColor:ccWHITE]];
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
    
	// don't forget to call "super dealloc"
//	[super dealloc];
}
@end;
