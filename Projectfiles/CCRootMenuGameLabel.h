//
//  CCRootMenuGameLabel.h
//  NewQuickWords
//
//  Created by Logen Watkins on 9/16/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface CCRootMenuGameLabel : CCMenuItemImage {
    NSNumber *matchID;
}

@property (nonatomic, retain) NSNumber *matchID;

- (void) loadLocalVars:(NSNumber *)pMatchID;
+ (id) initWithImage:(NSString *)normImage selectedImage:(NSString *)selImage string1:(NSString *)string1 string2:(NSString *)string2 matchID:(NSNumber *)matchID target:(id)r selector:(SEL)s;

@end
