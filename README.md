# QuickWords #

This app (never finished) is based on a road-trip game where the player is given a few letters as a prompt, and they must come up with as many words as possible that use the prompt letters in order.

* User is given a 3-letter prompt, such as "NTC".
* User can guess as many words as possible that contain the letters "N", "T", and "C" (in order) within the time limit.
* Examples of valid guesses for "NTC": "notice", "fantastic", "nautical", "antic"
 
### Features ###

* Challenge a friend to a best of 3 match for the highest score! (local only).
* Have multiple active matches -- stop and pick up where you left off earlier.
* Score recaps for each match, with a history view for looking over earlier matches.

### Tech Used ###

This app was developed in 2012, so multiple tech decisions are outdated by current standards.

* Objective-C, Xcode, iPhone simulator and actual device.
* Kobold2D (a now-deprecated branch from cocos2d) for all scenes/layers and game loop.
* MagicalRecord, an ActiveRecord-inspired library intended to clean up CoreData code. (https://github.com/magicalpanda/MagicalRecord)
* TestFlight

### Note on project status ###

* This project was never completed, although all the basics (creating games, playing games and basic scoring, recaps/side-by-side scores, history) were working.
* This project was not maintained in Git -- development began and ended around September 2012.
* Because of it's age, it's very likely it would require a fair amount of effort to get running.